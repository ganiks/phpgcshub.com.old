---
layout: page
title: About
group: navigation
---
{% include JB/setup %}

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">关于作者</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-9">
            <p> 当初的愿望实现了吗？世到如今只好祭奠吧。</p>
            <br/>
            <p> 不知不觉走上了Web开发道路的80后程序员...</p>
        </div>
        <div class="col-md-3">
            <img src="http://img3.douban.com/icon/ul46036943-2.jpg" class="img-rounded pull-right" alt="ganiks">
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">关于本站</h3>
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <tbody>
                <thead>
                    <th>Function</th>
                    <th align="left">Realization</th>
                </thead>
                <tr>
                    <td>blog framework</td>
                    <td align="left">jeklly</td>
                </tr>
                <tr>
                <td>blog helper</td>
                <td align="left">jeklly-bootstrap</td>
                </tr>
                <tr>
                <td>trusteeship</td>
                <td align="left">github pages</td>
                </tr>
                <tr>
                <td>front-end</td>
                <td align="left">bootstrapV3.0</td>
                </tr>
                <tr>
                <td>text language</td>
                <td align="left">markdown/html</td>
                </tr>
                <tr>
                <td>template language</td>
                <td align="left">liquid</td>
                </tr>
                <tr>
                <td>editor tools</td>
                <td align="left">vim</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">社交帐号</h3>
    </div>
    <div class="panel-body">
        <ol class="breadcrumb">
            <li>
                <span class="icon-weibo icon-large">&nbsp;<a href="http://weibo.com/phpgcs" target="_blank">@ganiks</a></span>
            </li>
            <!--li>
                <span class="icon-renren icon-large">&nbsp;<a href="http://renren.com/236570181" target="_blank">@ganiks</a></span>
            </li>
            <li>
                <span class="icon-pinterest icon-large">&nbsp;<a href="http://t.qq.com/ganiks" target="_blank">@ganiks</a></span>
            </li-->
            <li>
                <span class="icon-facebook icon-large">&nbsp;<a href="http://facebook.com/ganiksliu" target="_blank">@ganiksLiu</a></span>
            </li>
            <li>
                <span class="icon-twitter icon-large"><a href="http://twitter.com/ganiksliu" target="_blank">@ganiksLiu</a></span>
            </li>
            <li>
                <span class="icon-github icon-large">&nbsp;<a href="http://github.com/phpgcs" target="_blank">@phpgcs</a></span>
            </li>
            <li>
                <span class="icon-bitbucket icon-large">&nbsp;<a href="http://bitbucket.org/phpgcs" target="_blank">@phpgcs</a></span>
            </li>
            <li>
                <img src="http://blog.csdn.net/favicon.ico" />&nbsp;<a href="http://blog.csdn.net/phpgcs" target="_blank">csdn blog</a></span>
            </li>
        </ol>
    </div>
</div>

