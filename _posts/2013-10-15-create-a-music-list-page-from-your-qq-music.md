---
layout: post
tagline: "create a music list page from your QQ music"
title: "从QQ歌单创建你自己的音乐页面"
description: "从QQ歌单创建你自己的音乐页面"
keywords: "QQ music, JSONP"
category: code
tags: [jsonp, api]
---
{% include JB/setup %}


demo: [http://phpgcs.com/projects/music](http://phpgcs.com/projects/music)

<!-- more -->
> 什么都想定制成自己的，这也是我不知不觉走上程序员道路的原因吗？

重点：

1. QQ music api

2. JQuery JSONP


首先要有QQ音乐的API：
------------

参考：[http://www.cnblogs.com/Xer-Lee/archive/2012/05/18/2508141.html](http://www.cnblogs.com/Xer-Lee/archive/2012/05/18/2508141.html)

    HTTP请求格式：GET

    返回格式：json

    API地址：http://qzone-music.qq.com/fcg-bin/fcg_music_fav_getinfo.fcg

        参数 意义 备注

        dirinfo 是否返回用户音乐列表名称与id 为1时返回，为0时不返回

        dirid QQ音乐用户的列表id 

        uin 用户的QQ号码 

        p 随机函数生成的随机数 目前没发现有什么作用随便输一个数就好

    例子：

`GET http://qzone-music.qq.com/fcg-bin/fcg_music_fav_getinfo.fcg?dirinfo=0&dirid=201&uin=823011051`

后记：
----

用最新的QQ官方的API

[http://wiki.open.qq.com/wiki/v3/music/get_list](http://wiki.open.qq.com/wiki/v3/music/get_list)

然后，得到QQ音乐列表的JSON 数据如下格式：
-----

``` json 

jsonCallback({
    code:0,
    msg:"",
    uin:81549201,
    DirList:[
        {
            DirID:1,                //列表id
            DirName:"我最爱听",//列表名
            DirShow:1,//是否开放，1是，0否
            DirTime:1323078355//时间
        }
    ],
    SongDirID:2,//返回的列表id
    SongNum:1,//列表内歌曲数量
    CurNum:1,//当前播放歌曲排序
    SongList:[
        {
            i:1,//列表内歌曲的id
            type:13,//类型
            id:680277,//歌曲在曲库中id
            songname:"跨时代",//歌名
            singerid:4558,//歌手id
            singername:"周杰伦",//歌手名
            url:"http://stream5.qqmusic.qq.com/12680277.wma",歌曲地址
            diskid:56705,//专辑id
            diskname:"跨时代",//专辑名
            playtime:194//歌曲长度
        }
    ]
})
```

第三，开始写web
----

我的博客是用markdown写的，非常高兴可以嵌套使用 HTML

但是遇到处理JSON 数据，我自然就思维导向到PHP，但是在我目前的blog中是不能用一句PHP 的。。。

于是搜索关键字，寄希望与 jekyll/liquid /markdown 。。。无果

文章 [用jekyll生成json](http://yanping.me/cn/blog/2012/04/19/jekyll-with-json/) 给了启示：

我居然忘记了JQuery ！！！该打 ！！

开始，

``` javascript

<div id="music_list"></div>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
var url = "http://qzone-music.qq.com/fcg-bin/fcg_music_fav_getinfo.fcg?dirinfo=0&dirid=201&uin=823011051";
$(document).ready(function(){
    $.ajax({
        type: 'get',
        url: url,
        async: false,
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'jsonCallback',
        success: function(data){
            var list = '<ul class="music_list">';
            $.each(data.SongList, function(i, item){
                console.log(item.songname);
                list += '<li><span>' + item.songname + '</span></li>';
            });
            list += '</ul>';
            $('#music_list').append(list);
        }
    });
});
</script>
```
> Javascript 是基于jekyll的博客系统跟动态数据交互的唯一方式吗？希望不是..

最后要说说 AJAX跨域问题和JSONP
-----

> 说到AJAX就会不可避免的面临两个问题，第一个是AJAX以何种格式来交换数据？第二个是跨域的需求如何解决？这两个问题目前都有不同的解决方案，比如数据可以用自定义字符串或者用XML来描述，跨域可以通过服务器端代理来解决。

> 但到目前为止最被推崇或者说首选的方案还是用JSON来传数据，靠JSONP来跨域

如果用常规的 .getJSON(url, function(){}) 去请求的话，会收到提示：

    XMLHttpRequest cannot load http://qzone-music.qq.com/fcg-bin/fcg_music_fav_getinfo.fcg?dirinfo=0&dirid=201&uin=823011051 Origin http://localhost:4000 is not allowed by Access-Control-Allow-Origin. 

参考：

[Origin http://localhost is not allowed by Access-Control-Allow-Origin.?](http://stackoverflow.com/questions/12683530/origin-http-localhost-is-not-allowed-by-access-control-allow-origin)

[http://www.cnblogs.com/dowinning/archive/2012/04/19/json-jsonp-jquery.html](http://www.cnblogs.com/dowinning/archive/2012/04/19/json-jsonp-jquery.html)


