---
layout: post
title: "yii Global Variables"
description: "yii Global Variables"
keywords: "yii,Global,variable"
category: Yii
tags: []
---
{% include JB/setup %}

Global Variable

<!-- more -->

``` php
<?php 
// config/main.php

'params'=>array(
'adminEmail'=>'webmaster@example.com',
'sentimentValue'=>array('zm'=>array(0.01,10.00),'zx'=>array(-0.00,0.00),'fm'=>array(-10.00,-0.01)),
),


$zm = Yii::app()->params['sentimentValue']['zm'];
$zx = Yii::app()->params['sentimentValue']['zx'];
$fm = Yii::app()->params['sentimentValue']['fm']; 

In Yii, you can do achieve this by making a class (under protected/compoents) which inherits CApplicationComponent class. 
And then you call any property of this class globally as a component.

class GlobalDef extends CApplicationComponent {
 public $aglobalvar;
}

Define this class in main config under components as:
'globaldef' => array('class' => 'application.components.GlobalDef '),

And you can call like this:
echo Yii::app()->globaldef->aglobalvar;
```
