---
layout: post
title: "JQuery入门基础"
keywords: "JQuery入门基础"
description: "JQuery入门基础"
category: code
tags: [jquery, getting-started]
---
{% include JB/setup %}

<!-- more -->
jQuery 选择器
--
``$(this) $("p") p.class .class #id $("ul li:first") $("[href$='.jpg']") $("div#id .class")``

jQuery 事件
--
    由于 jQuery 是为处理 HTML 事件而特别设计的，把所有 jQuery 代码置于事件处理函数中,置于文档就绪事件处理器中
jQuery 效果
---

jQuery 隐藏/显示

+ `$("p").hide(speed, callback)`
+ `$("p").show(speed, callback)`
+ `$("p").toggle(speed, callback)`
+ speed= fast/slow/1000(ms);

jQuery 淡入淡出

+ `$("p").fadeIn(speed, callback)`
+ `$("p").fadeOut(speed, callback)`
+ `$("p").fadeToggle(speed, callback)`
+ `高级淡出效果 fadeTo(speed, opacity, callback) opacity介于[0, 1]之间`
+ speed= fast/slow/1000(ms);

jQuery 滑动

+ `$("p").slideDown(speed, callback)`
+ `$("p").slideUp(speed, callback)`
+ `$("p").slideToggle(speed, callback)`
+ speed= fast/slow/1000(ms);

jQuery 动画 

+ `$("div").animate({left:'250px', opacity:'0.5', height:'+=150px', width:'200px'});`
+ 对于 left属性，需要设置 position为 absolute/relative/fixed;
+ 还可以设置 height:'+=150px',或者 预定义的值 height:'toggle/show/hide'

jQuery stop()   

+ ``$(selector).stop(stopAll,goToEnd);``

jQuery Callback 

+ ``Callback 函数在当前动画 100% 完成之后执行``

jQuery Chaining  

+ ``$("#p1").css("color","red").slideUp(2000).slideDown(2000);``

jQuery HTML
--
jQuery 获取  ``.text() .html() .val() 获取表单字段的值  .attr("href")  .css("height", "200px")``

jQuery 设置  ``.text()等 有回调函数  $("#text1").text(funtion(i, origText){ return origText+"/jquery";});  //其中 i 是被选元素的下标``
.attr() 也有回调函数 $("#a1").attr("href", function(i, origVal){}));
.attr() 可以同时设置多个参数  .attr({ "href": "http://www.badi.com", "title": "21234"});
jQuery 添加

append() - 在被选元素的结尾插入内容  $("p").append("Some appended text.");
prepend() - 在被选元素的开头插入内容  $("p").prepend("Some prepended text.");
after() - 在被选元素之后插入内容           $("img").after("Some text after");
before() - 在被选元素之前插入内容        $("img").before("Some text before");
var txt1="<p>Text.</p>";               // 以 HTML 创建新元素
var txt2=$("<p></p>").text("Text.");   // 以 jQuery 创建新元素
var txt3=document.createElement("p");  // 以 DOM 创建新元素    
 

 

jQuery 删除

$("#div1").remove(); 
$("#div1").empty(); 
$("#div1").remove(".class"); 
jQuery CSS 类

addClass() - 向被选元素添加一个或多个类 $("#div1").addClass("important blue");
removeClass() - 从被选元素删除一个或多个类 $("#div1").removeClass("important blue");
toggleClass() - 对被选元素进行添加/删除类的切换操作
css() - 设置或返回样式属性
 

jQuery css()

jQuery 尺寸

innerWidth() 方法返回元素的宽度（包括内边距）。

innerHeight() 方法返回元素的高度（包括内边距）

 

width() 方法设置或返回元素的宽度（不包括内边距、边框或外边距）。

height() 方法设置或返回元素的高度（不包括内边距、边框或外边距）

outerWidth() 方法返回元素的宽度（包括内边距和边框）。

outerHeight() 方法返回元素的高度（包括内边距和边框）。

outerWidth(true) 方法返回元素的宽度（包括内边距、边框和外边距）。

outerHeight(true) 方法返回元素的高度（包括内边距、边框和外边距）。

jQuery 遍历

jQuery 遍历

jQuery 祖先

jQuery 后代

jQuery 同胞

jQuery 过滤

jQuery AJAX

jQuery AJAX 简介

jQuery 加载

jQuery Get/Post

jQuery 杂项

jQuery noConflict()

jQuery 实例

jQuery 实例

jQuery 测验

jQuery 参考手册

jQuery 参考手册

jQuery 选择器

jQuery 事件

jQuery 效果

jQuery 文档操作

jQuery 属性操作

jQuery CSS 操作

jQuery Ajax

jQuery 遍历

jQuery 数据

jQuery DOM 元素

jQuery 核心

jQuery 属性

 
