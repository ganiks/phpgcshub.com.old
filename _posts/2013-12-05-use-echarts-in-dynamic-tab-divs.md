---
layout: post
title: "use echarts in dynamic tab divs"
description: ""
keywords: ""
category: code 
tags: []
---
{% include JB/setup %}

在动态Div中（如标签页Tabs）中使用`echarts`时，

<!-- more -->

Tab1 Tab2 Tab3
开始默认Tab1 是激活状态的active

1. div 共用 毫无疑问，需要重绘(绑定重绘函数到tabs.on('show', function(){}))
2. div 不共用
    1. Tab2 能否在未激活之前render echarts(我们知道普通的Div是可以的), 如果可以，就一次性render，切换Tab就不用做操作了
    2. 切换到Tab2再回到Tab1 后，Tab1 的 echarts 是否要重绘
        1. 重绘的话，共用chart实例？
        1. 重绘的话，共用option?


共用div 
    好处：html 内容少，不用定义那么多div和id，如果3*3个Tab就是9个Div，如果都不共用的话，只用1个Div
    好处：render的时候，都只需要render在一个Div上, 否则会这样 $('#sentiment_'+type+sentiment+line_or_pie).html(string);(快晕了)
    好处：一次render,一次性
    坏处：切换Div都要重新 .html() 或者 .setOption()，不过可以将data放在 var cache = {}中来解决
    坏处：要帮定很多的.show在tab上
