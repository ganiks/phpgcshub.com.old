---
layout: post
title: "jquery dataTables plugin conflict with AMD common.js"
description: ""
keywords: ""
category: code 
tags: []
---
{% include JB/setup %}

<!-- more -->

``` php
<?
 Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/js/jquery.dataTables.js', CClientScript::POS_END);
 
 //esl.js AMD must placed after jquery.dataTables.js, otherwise conflicts , dataTables wouldn't be initialize normally
 Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/ebucharts/esl.js', CClientScript::POS_END);
 Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/ebucharts/echarts-map.js', CClientScript::POS_END);

 Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/js/DT_bootstrap.js', CClientScript::POS_END);


``` 

PS: 

著名的瀑布流插件 Masonry也跟AMD规范冲突

http://masonry.desandro.com

