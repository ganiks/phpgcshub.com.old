---
layout: post
title: "screen config"
description: ""
keywords: ""
category: code 
tags: []
---
{% include JB/setup %}

<!-- more -->

``` sh

  0 # 启动时不显示欢迎屏幕
  1 startup_message off
  2 
  3 # 定义screen的功能键为Ctrl-Z。向终端输入Ctrl-Z时应按 Ctrl-Z z。
  4 escape ^Zz
  5 
  6 # 屏幕缓冲区1000行。
  7 defscrollback 1000
  8 
  9 # 在最下一行显示窗口列表和时钟
 10 hardstatus on
 11 hardstatus alwayslastline
 12 #hardstatus string "%{.bW}%-w%{.rY}%n %t%{-}%+w %=%{..G} %{..Y} %Y/%m/%d %c:%s"
 13 hardstatus string '%{gk}[ %{G}%H %{g}][%= %{wk}%?%-Lw%?%{=b kR}(%{W}%n*%f %t%?(%u)%?%{=b kR})%{= kw}%?%+Lw%?%?%= %{g}]%{=b C}[ %m/%d %c:%s ]%{W}'
 14 
 15 # 关闭错误提示
 16 vbell off
 17 
 18 # 按 Ctrl-Z w 或 Ctrl-Z Ctrl-W 显示窗口列表
 19 bind w windowlist -b
 20 bind ^w windowlist -b
 21 
 22 # 进程挂起时自动detach
 23 autodetach on
 24 
~                       
``` 
