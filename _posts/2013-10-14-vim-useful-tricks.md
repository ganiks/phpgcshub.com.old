---
layout: post
title: "vim useful tricks"
description: "vim useful tricks"
keywords: "vim"
category: code
tags: [vim]
---
{% include JB/setup %}

本文整理Vim 不常用但是非常有用的技巧

<!-- more -->

delete blank lines
--

`:g/^$/d`


save files without root's permissions
--

`:w !sudo tee %`

force someone logout
--

`who`
`pkill -kill -t pts/1`

add comments to multiple lines 
--

ctrl+v
shift+i(I)
(insert a #)
ESC
