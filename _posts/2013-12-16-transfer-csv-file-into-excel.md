---
layout: post
title: "transfer csv file into excel"
tagline: "转换CSV文件为Excel"
description: "转换CSV文件为Excel"
keywords: "csv,excel,php"
category: php
tags: [excel]
---
{% include JB/setup %}


项目中当时基于anycharts画了曲线图，客户要下载画图的原始数据，格式为excel。

<!-- more -->

由于anycharts的API要的是Xml，当时用程序将数据统计完毕后放入了一个csv文件供生成xml。

直接将csv数据读出稍微修饰，存入excel，想必要比重新统计来的快吧。

``` php

<?
//controller

public function actionExportLineData()
{
    $cookie = Yii::app()->request->getCookies();
    if($cookie['KDValue']) $KDValue= $cookie['KDValue']->value;
    exec(Yii::app()->basePath."/yiic m_event exportLinedata --KDValue='$KDValue'", $out, $status);
    if($status === 0)
    {
        $response = explode('|', substr(Yii::app()->basePath, 0, -9).'/'.$out[0]);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$response[1].'"');
        header('Cache-Control: max-age=0');
        readfile("$response[0]"); 
    }
    else
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="error.xls"');
        header('Cache-Control: max-age=0'); 
    }
}

?>


<?
//yii commands
public function actionExportLinedata($KDValue)
{
    $errorHandle = FALSE;
    //找到并读取 csv 文件
    $savePath = substr(yii::app()->basePath, 0, -9)."data/modules/event/flash/line/{$KDValue}/";
    $csvname  = 'csco_daily.csv';
    $file = file($savePath.$csvname);
    
    //生成 excel
    if($file){
        //禁用自动加载类,后面会恢复
        spl_autoload_unregister(array('YiiBase','autoload'));

        //加载PHPExcel
        $phpExcelPath = Yii::getPathOfAlias('system.vendors');
        include_once($phpExcelPath.DIRECTORY_SEPARATOR.'phpexcel/Classes/PHPExcel.php');

        //缓存设置
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize'=>'1024MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        //begin
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "¿?¿?");
        $objPHPExcel->getActiveSheet()->setCellValue('B1', "¿?¿?¿?¿?(¿?¿?)");
        $objPHPExcel->getActiveSheet()->setCellValue('C1', "¿?¿?¿?¿?(¿?¿?)");
        $objPHPExcel->getActiveSheet()->setCellValue('D1', "¿?¿?¿?¿?(¿?¿?)");
        $objPHPExcel->getActiveSheet()->setCellValue('E1', "¿?¿?");
        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray(
            array('font'=>array('bold'=>true))
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(17);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->freezePane('A1');
        $objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);
        
        $i = 1;
        //去除第一行csv数据
        array_shift($file);

        //拼装数据
        foreach($file as $key=>$val)
        {
            $array = split(',', $val);
            $rawKey = $i +1;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rawKey, $array[0])
                ->setCellValue('B'.$rawKey, $array[2])
                ->setCellValue('C'.$rawKey, $array[3])
                ->setCellValue('D'.$rawKey, $array[4])
                ->setCellValue('E'.$rawKey, $array[5]);
            $i++;
        }
        $objPHPExcel->getActiveSheet()->setTitle('¿?¿?¿?');

        //回复自动加载类
        spl_autoload_register(array('YiiBase','autoload'));

        Yii::import('application.components.FileHelper');
        $basePath = substr(yii::app()->basePath, 0, -9);
        $downPath = 'data/modules/event/excel/'.date('Y-m-d', time()).'/';
        if(FileHelper::createFolder($basePath.$downPath))
        {
            $filename = date('Y-m-d', time()).'_'.md5(time()).'.xls';
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save($basePath.$downPath.$filename);
        }
        else
        {
            $errorHandle = TRUE;
        }
    }
    else 
    {
        $errorHandle = TRUE;
    }
    if($errorHandle) echo 'FAILD';
    else echo $downPath.$filename.'|'.$filename."\r\n";
}

?>

```

关于禁用自动装载Yii类
--

如果没有禁用的话，yii跟phpexcel 的自动装载会冲突：

``` 
PHP Error[2]: include(PHPExcel_Shared_ZipStreamWrapper.php): failed to open stream: No such file or directory
    in file /home/framework/yii-1.1.10.r3566/framework/YiiBase.php at line 418
    #0 /home/framework/yii-1.1.10.r3566/framework/YiiBase.php(418): autoload()
    #1 unknown(0): autoload()
    #2 /home/framework/yii-1.1.10.r3566/framework/vendors/phpexcel/Classes/PHPExcel/Autoloader.php(31): spl_autoload_call()
    #3 /home/framework/yii-1.1.10.r3566/framework/vendors/phpexcel/Classes/PHPExcel.php(32): require()
    #4 /var/www/webinfosys/protected/commands/M_eventCommand.php(809): include_once()
    #5 unknown(0): M_eventCommand->actionExportLinedata()
    #6 /home/framework/yii-1.1.10.r3566/framework/console/CConsoleCommand.php(141): ReflectionMethod->invokeArgs()
    #7 /home/framework/yii-1.1.10.r3566/framework/console/CConsoleCommandRunner.php(65): M_eventCommand->run()
    #8 /home/framework/yii-1.1.10.r3566/framework/console/CConsoleApplication.php(91): CConsoleCommandRunner->run()
    #9 /home/framework/yii-1.1.10.r3566/framework/base/CApplication.php(162): CConsoleApplication->processRequest()
    #10 /home/framework/yii-1.1.10.r3566/framework/yiic.php(33): CConsoleApplication->run()
    #11 /var/www/webinfosys/protected/yiic.php(7): require_once()
    #12 /var/www/webinfosys/protected/yiic(4): require_once()

``` 

不过也可以去修改phpexcel的源码来解决这个问题

``` php
<?
/*

if (function_exists('__autoload')) {
//    Register any existing autoloader function with SPL, so we don't get any clashes
    spl_autoload_register('__autoload');
}
//    Register ourselves with SPL
    return spl_autoload_register(array('PHPExcel_Autoloader', 'Load'));

*/

$functions = spl_autoload_functions();
foreach ( $functions as  $function)
    spl_autoload_unregister($function);
$functions = array_merge(array(array('PHPExcel_Autoloader','Load')),$functions);
foreach ( $functions as $function)
    $x = spl_autoload_register($function);
return $x;

}    //    function Register()
```

SPL 函数
--

* class_implements — 返回指定的类实现的所有接口。
* class_parents — 返回指定类的父类。
* class_uses — Return the traits used by the given class
* iterator_apply — 为迭代器中每个元素调用一个用户自定义函数
* iterator_count — 计算迭代器中元素的个数
* iterator_to_array — 将迭代器中的元素拷贝到数组
* spl_autoload_call — 尝试调用所有已注册的__autoload()函数来装载请求类
* spl_autoload_extensions — 注册并返回spl_autoload函数使用的默认文件扩展名。
* spl_autoload_functions — 返回所有已注册的__autoload()函数。
* spl_autoload_register — 注册__autoload()函数
* spl_autoload_unregister — 注销已注册的__autoload()函数
* spl_autoload — __autoload()函数的默认实现
* spl_classes — 返回所有可用的SPL类
* spl_object_hash — 返回指定对象的hash id
