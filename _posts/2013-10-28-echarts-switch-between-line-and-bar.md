---
layout: post
title: "echarts switch between line and bar"
tagline: "customize your line/bar"
description: ""
keywords: "echarts MagicType"
category: code 
tags: [echarts]
---
{% include JB/setup %}

echarts have a feature named "magicType" which allow users switch the chart's type between line and bar.

<!-- more -->

Today I have a problem :

I want the line to be none-stacked while the bar stacked on the same chart.

We know that the "stack" property was defined as below:

`stack: function(){ return parseInt(line_or_bar) ? null : "总量";}(), `

or simply like this:

`stack: "总量", ` and it's stacked

`stack: null, ` and it's none-stacked

ok , when I render the chart first time , I can define it's stack property by a variable `line_or_bar`

however, when we click the switch-button "MagicType_line" or "MagicType_bar", the `option.data[x].stack` value won't change !

this is the problem.

to sovle it , I use the function `on` with api-function `magicTypeChanged`


``` javascript
//here I render 2 lines , so I use line[type]
line[type].setOption(lineOption[type], true);

//下面的方法是为了在同一个chart上面实现line_stack_no和bar_stack_yes

line[type].on('magicTypeChanged', function(e){
    //console.log(e);//check what's the e object
    for(var x in lineOption[type].series){
        if(e.magicType == "line"){
            lineOption[type].series[x].stack = null;
            lineOption[type].series[x].type = "line";
            lineOption[type].xAxis[0].boundaryGap = false;
            lineOption[type].xAxis[0].splitArea = true;
        }
        else{
            lineOption[type].series[x].stack = "aa";
            lineOption[type].series[x].type = "bar";
            lineOption[type].xAxis[0].boundaryGap = true;
            lineOption[type].xAxis[0].splitArea = false;
        }
    }
    line[type].setOption(lineOption[type], true);
});  
```
