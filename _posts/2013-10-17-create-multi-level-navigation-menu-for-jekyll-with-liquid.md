---
layout: post
title: "jekyll创建多级导航菜单"
tagline: "create multi level navigation menu for jekyll with liquid"
description: "create multi level navigation menu for jekyll with liquid"
keywords: "create multi level navigation menu for jekyll with liquid"
category: code
tags: [liquid, explore, jekyll]
---
{% include JB/setup %}

create multi level navigation menu for jekyll with liquid

<!-- more -->

demands：
--

I want pages `archive, tags, category` showed as a 2nd-level menu item in my jekyll blog's navigation-bar, the same time form their 1st-level menu item `Archives`

then, when I rake a new 2-nd-level page ,I'll define the YAML header like this:

    ---
    layout: page
    title: Categories
    group2: "Archives"
    ---
    {% include JB/setup %}

while the 1st-level page, still this:

    ---
    layout: page
    title: About
    group: "navigation"
    ---
    {% include JB/setup %}


demo:
--
<http://www.phpgcs.com>

priciple:
--
modify your liquid templates

Steps 1: create liquid template ``_includes/JB/navs_list``
--

{% raw %}

    {% if site.JB.navs_list.provider == "custom" %}

      {% include custom/navs_list %}

    {% else %}

      {% for node in pages_list %}

        {% if node.title != null %}

          {% if group == null or group == node.group %}

            {% if page.url == node.url %}

            <li class="active"><a href="{{ BASE_PATH }}{{node.url}}" class="active">{{node.title}}</a></li>

            {% else %}

            <li><a href="{{ BASE_PATH }}{{node.url}}">{{node.title}}</a></li>

            {% endif %}

          {% endif %}

        {% endif %}

      {% endfor %}

    {% endif %}

    {% assign pages_list = nil %}

{% endraw %}

Step 2:  create liquid template ``_includes/custom/navs_list``
----

{% raw %}

    {% for nav2 in site.JB.navs_list.navigation2 %}
        {% if nav2 == current_page  %}
            <li class="dropdown active">
        {% else %}
            <li class="dropdown">
        {% endif %}
            <a href="{{ BASE_PATH }}{{node.url}}" class="dropdown-toggle" data-toggle="dropdown">
                {{ nav2 }}
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                {% for node in pages_list %}
                    {% if node.title != null and node.group2 == nav2 %}
                        {% if page.url == node.url %}
                            <li class="active"><a href="{{ BASE_PATH }}{{node.url}}" class="active">{{node.title}}</a></li>
                        {% else %}
                            <li><a href="{{ BASE_PATH }}{{node.url}}">{{node.title}}</a></li>
                        {% endif %}
                    {% endif %}
                {% endfor %}
            </ul>
        </li>
    {% endfor %}
    {% for node in pages_list %}
        {% if node.title != null %}
          {% if group == null or group == node.group or node.group2 != nil %}
            {% if node.group2 == null %}
                {% if page.url == node.url %}
                <li class="active"><a href="{{ BASE_PATH }}{{node.url}}" class="active">{{node.title}}</a></li>
                {% else %}
                <li><a href="{{ BASE_PATH }}{{node.url}}">{{node.title}}</a></li>
                {% endif %}
            {% endif %}
          {% endif %}
        {% endif %}
    {% endfor %}
    {% assign pages_list = nil %}
    {% assign group = nil %}
    {% assign group2 = nil %}

{% endraw %}

Step 3: assign the vars in your layout_file `default.html` which render the navigation menu
-----

{% raw %}
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          {% assign pages_list = site.pages %}
          {% assign group = 'navigation' %}
          {% assign current_page = page.url | remove_first: '/' | split: '/' | first | capitalize %}
          {% include JB/navs_list %}
          <li>
            <form class="form-inline visible-lg visible-md" role="form" onsubmit="tmtxt_search_google()">
              <input type="text" class="form-control input-sm" placeholder="Google Search" id="my-google-search">
            </form>
          </li>
        </ul>

{% endraw %}

Step 4:  define the `navs_list.group2` in your `_config.yml`
--------

      navs_list:
        provider: custom
        navigation2: ["Archives", "Projects"]


