---
layout: post
category : code
title: "Shell Programe"
tagline: "Shell编程基础"
tags : [shell, getting-started]
---
{% include JB/setup %}

shell, bash, sh ...

<!-- more -->

shell 扫盲:
==

+ 在计算机科学中，Shell俗称壳（用来区别于核），是指“提供使用者使用界面”的软件（命令解析器）。
+ 它接收用户命令，然后调用相应的应用程序;  同时它又是一种程序设计语言。
+ 作为命令语言，它交互式解释和执行用户输入的命令或者自动地解释和执行预先设定好的一连串的命令；
+ 作为程序设计语言，它定义了各种变量和参数，并提供了许多在高级语言中才具有的控制结构，包括循环和分支。


基本上shell分两大类：
----

一：图形界面shell（Graphical User Interface shell 即 GUI shell）

+ 用最为广泛的 Windows Explorer （微软的windows系列制作系统）
+ Linux shell
    + window manger (BlackBox和FluxBox）
    + CDE
    + GNOME
    + KDE
    + XFCE

二：命令行式shell（Command Line Interface shell ，即CLI shell）

+ bash
+ sh
+ ksh
+ csh（Unix/linux 系统） （MS-DOS系统）
+ cmd.exe/ 命令提示字符（Windows NT 系统） 
+ Windows PowerShell（支援 .NET Framework 技术的 Windows NT 系统）

    传统意义上的shell指的是命令行式的shell，以后如果不特别注明，shell是指命令行式的shell。

在UNIX中主要有两大类shell
----

* Bourne shell （包括 sh,ksh,and bash)
    * Bourne shell ( sh)
    * Korn shell ( ksh)
    * Bourne Again shell ( bash)
    * POSIX shell ( sh)
* C shell （包括 csh and tcsh)
    * C shell ( csh)
    * TENEX/TOPS C shell ( tcsh)

Bourne Shell
--

首个重要的标准Unix Shell是1970年底在V7 Unix(AT&T第7版）中引入的，并且以它的创始科技部基础条件平台“国家气象网络计算应用节点建设”（2004DKA50730）资助者Stephen Bourne的名字命名。

Bourne shell 是一个交换式的命令解释器和命令编程语言。Bourne shell 可以运行为login shell或者login shell的子shell(subshell）。

只有login命令可以调用Bourne shell作为一个login shell。

此时，shell先读取/etc/profile文件和$HOME/.profile文件。

/etc/profile文件为所有的用户定制环境，$HOME/.profile文件为本用户定制环境。最后，shell会等待读取你的输入。

Bourne Again Shell (`bash`)
--

bash是GNU计划的一部分，用来替代Bourne shell。

它用于基于GNU的系统如Linux。

大多数的Linux(Red Hat,Slackware,Caldera）都以`bash`作为缺省的shell，并且运行`sh`时，其实调用的是`bash`。

各主要操作系统下缺省的shell:
--

* AIX下是Korn Shell.
* Solaris缺省的是Bourne shell.
* FreeBSD缺省的是C shell
* HP-UX缺省的是POSIX shell.
* Linux是Bourne Again shell.

Bash 常识
==

``` bash
#!/bin/bash
...
```
1. 它必须以如下行开始（必须放在文件的第一行）：
    注意：最好使用“!/bin/bash”而不是“!/bin/sh”，如果使用tc shell改为tcsh，其他类似。
    符号#!用来告诉系统执行该脚本的程序，本例使用/bin/bash。

2. 编辑结束并保存后，如果要执行该脚本，必须先使其可执行： `chmod +x filename`
3. 此后在该脚本所在目录下，输入 `./filename` 即可执行该脚本。


Shell 代码实战：
==

``` bash

1. 变量赋值和引用
2. Shell里的流程控制
    2.1. if 语 句
    2.2. && 和 || 操作符
    2.3. case 语句
    2.4. select 语句
    2.5. while/for 循环
3. Shell里的一些特殊符号
    3.1. 引号
4. Here Document
5. Shell里的函数
6. 命令行参数
7. Shell脚本示例
    7.1. 一般编程步骤
    7.2. 二进制到十进制的转换
    7.3. 文件循环拷贝
8. 脚本调试

#!/bin/bash

echo "refer:"
echo "http://wiki.ubuntu.org.cn/Shell%E7%BC%96%E7%A8%8B%E5%9F%BA%E7%A1%80"

echo "#############################"
echo "Example 1: if sentence"

if [${SHELL} = "/bin/bash"]; then
    echo "your login shell is bash (bourne again shell)"
else
    echo "your login shell is not bash but ${SHELL}"
fi


#注意这里[ ... ]内部前后的空格
if [ ${SHELL} = "/bin/bash" ]; then
    echo "your login shell is bash (bourne again shell)"
else
    echo "your login shell is not bash but ${SHELL}"
fi

echo "#############################"
echo "Example 2: more conditions if sentence"

if [ -f "aaaa" ]; then
    echo "aaaa is a file"
else
    echo "aaaa is not a file"
fi

if [ -f "first" ]; then
    echo "first is a file"
else
    echo "first is not a file"
fi

if [ -x "aa" ]; then
    echo "aa is a excuable file"
else
    echo "aa is not a excuable file"
fi

if [ -x "first" ]; then
    echo "first is a excuable file"
else
    echo "first is not a excuable file"
fi

if [ -n "$a" ]; then
    echo "a has value $a "
else
    echo "a doesn't have value"
fi

a=2
if [ -n "$a" ]; then
    echo "a has value $a "
else
    echo "a doesn't have value"
fi

echo "######################################"
echo "Example 3: case sentence : an auto un-zip program"

ftype="$(file "$1")"
case "$ftype" in 
"$1: Zip archive"*)
    unzip "$1" ;;
"$1: gzip compressed"*)
    gunzip "$1" ;;
"$1: bzip2 compressed"*)
    bunzip2 "$1" ;;
*) echo "file $1 can not be uncompressed with smartzip";;
esac

echo "######################################"
echo "Example 4: select sentence"

echo "what is your favourite os ?"
select var in "linux" "windows" "freeBSD" ; do
    break;
done
echo "your favourite os is $var"

echo "######################################"
echo "Example 5: while/for sentence "

for var in A B C; do 
    echo "var is $var"
done

echo "######################################"
echo "Example 6: avoid * be explained with ' or \" "

echo *.jpg
echo '*.jpg'
echo "*.jpg"

echo "其中单引号更严格一些，它可以防止任何变量扩展；而双引号可以防止通配符扩展但允许变量扩展"
echo $SHELL
echo '$SHELL'
echo "$SHELL"
echo \*.jpg
echo \$SHELL


echo "######################################"
echo "Example 7:  "

```

<a target="_blank" href="http://wiki.ubuntu.org.cn/Shell%E7%BC%96%E7%A8%8B%E5%9F%BA%E7%A1%80">
参考： http://wiki.ubuntu.org.cn/Shell%E7%BC%96%E7%A8%8B%E5%9F%BA%E7%A1%80
</a>
