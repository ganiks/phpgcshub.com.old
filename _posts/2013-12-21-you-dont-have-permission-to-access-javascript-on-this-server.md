---
layout: post
title: "You don't have permission to access javascript on this server"
description: "You don't have permission to access javascript on this server"
keywords: "javascript,apache,ubuntu"
category: apache
tags: [bug]
---
{% include JB/setup %}

my god, this bug , how to say -- "亮瞎了我的双眼"

<!-- more -->

when I visited `http://localhost:9012/javascript/`, I got such error:

``` bash
Forbidden

You don't have permission to access /javascript/ on this server.

Apache/2.2.22 (Ubuntu) Server at localhost Port 9012

```

of course , I have checked the authority of the dir `javascript`

what's funny, I `move javascript js`, and `http://localhost:9012/js/` is accessable !!!

so , it's neither an authority problem, nor 9012 Visual Host config problem

the reason must be about the Apache

As a result, the problem was caused by apache config file `/etc/apache2/conf.d/javascript-common.conf` 

``` bash 
  0 Alias /javascript /usr/share/javascript/
  1
  2 <Directory "/usr/share/javascript/">
  3     Options FollowSymLinks MultiViews
  4 </Directory>

```
