---
layout: post
title: "origin http is not allowed by access control allow origin"
tagline: 跨域AJAX
description: "origin-http-is-not-allowed-by-access-control-allow-origin"
keywords: "jsonp,ajax"
category: code
tags: [bug, jquery, ajax]
---
{% include JB/setup %}

<!-- more -->
> 说到AJAX就会不可避免的面临两个问题

>> 第一个是AJAX以何种格式来交换数据？

>> 第二个是跨域的需求如何解决？

> 这两个问题目前都有不同的解决方案，比如数据可以用自定义字符串或者用XML来描述，跨域可以通过服务器端代理来解决。

> 但到目前为止最被推崇或者说首选的方案还是用JSON来传数据，靠JSONP来跨域

用常规的 .getJSON(url, function(){}) 去请求的话，会收到提示：

    XMLHttpRequest cannot load http://qzone-music.qq.com/fcg-bin/fcg_music_fav_getinfo.fcg?dirinfo=0&dirid=201&uin=823011051 Origin http://localhost:4000 is not allowed by Access-Control-Allow-Origin. 

原因如下：

You're getting this error because of XMLHttpRequest same origin policy, which basically boils down to a restriction of ajax requests to URLs with a different port, domain or protocol. This restriction is in place to prevent cross-site scripting (XSS) attacks.

具体可参考：

[http://www.w3.org/Security/wiki/Same_Origin_Policy](http://www.w3.org/Security/wiki/Same_Origin_Policy)

2种解决方案原理：

+ The proxy script works by tricking the browser, as you're actually requesting a page on the same origin as your page. The actual cross-origin requests happen server-side.
+ JSONP uses the ability to point script tags at JSON (wrapped in a javascript function) in order to receive the JSON. The JSONP page is interpreted as javascript, and executed. The JSON is passed to your specified function.

先说说proxy 的解决方案：

You can create a proxy script on the same domain as your website in order to avoid the cross-origin issues.

``` php

<?php
// File Name: proxy.php
if (!isset($_GET['url'])) die();
$url = urldecode($_GET['url']);
$url = 'http://' . str_replace('http://', '', $url); // Avoid accessing the file system
echo file_get_contents($url);
```
Then you just call this script with jQuery. Be sure to urlencode the URL.

``` javascript
$.ajax({
    url:"proxy.php?url=http%3A%2F%2Fapi.master18.tiket.com%2Fsearch%2Fautocomplete%2Fhotel%3Fq%3Dmah%26token%3D90d2fad44172390b11527557e6250e50%26secretkey%3D83e2f0484edbd2ad6fc9888c1e30ea44%26output%3Djson",
    type:'GET',
    dataType:"json",
    success:function(data){console.log(data.results.result[1].category);}
});
```


重点说说JSONP

实际应用时demo：

``` html

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml" >
 <head>
     <title>Untitled Page</title>
      <script type="text/javascript" src=jquery.min.js"></script>
      <script type="text/javascript">
     jQuery(document).ready(function(){ 
        $.ajax({
             type: "get",
             async: false,
             url: "http://flightQuery.com/jsonp/flightResult.aspx?code=CA1998",
             dataType: "jsonp",
             jsonp: "callback",//传递给请求处理程序或页面的，用以获得jsonp回调函数名的参数名(一般默认为:callback)
             jsonpCallback:"flightHandler",//自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名，也可以写"?"，jQuery会自动为你处理数据
             success: function(json){
                 alert('您查询到航班信息：票价： ' + json.price + ' 元，余票： ' + json.tickets + ' 张。');
             },
             error: function(){
                 alert('fail');
             }
         });
     });
     </script>
     </head>
  <body>
  </body>
 </html>

```

参考：

[Origin http://localhost is not allowed by Access-Control-Allow-Origin.?](http://stackoverflow.com/questions/12683530/origin-http-localhost-is-not-allowed-by-access-control-allow-origin)

[http://www.cnblogs.com/dowinning/archive/2012/04/19/json-jsonp-jquery.html](http://www.cnblogs.com/dowinning/archive/2012/04/19/json-jsonp-jquery.html)


最后引用原作者``dowinning``的几句话：

    1、ajax和jsonp这两种技术在调用方式上“看起来”很像，目的也一样，都是请求一个url，然后把服务器返回的数据进行处理，因此jquery和ext等框架都把jsonp作为ajax的一种形式进行了封装；

    2、但ajax和jsonp其实本质上是不同的东西。ajax的核心是通过XmlHttpRequest获取非本页内容，而jsonp的核心则是动态添加<script>标签来调用服务器提供的js脚本。

    3、所以说，其实ajax与jsonp的区别不在于是否跨域，ajax通过服务端代理一样可以实现跨域，jsonp本身也不排斥同域的数据的获取。

    4、还有就是，jsonp是一种方式或者说非强制性协议，如同ajax一样，它也不一定非要用json格式来传递数据，如果你愿意，字符串都行，只不过这样不利于用jsonp提供公开服务。

    总而言之，jsonp不是ajax的一个特例，哪怕jquery等巨头把jsonp封装进了ajax，也不能改变着一点！


