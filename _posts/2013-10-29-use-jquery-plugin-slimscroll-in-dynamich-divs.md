---
layout: post
title: "use jquery plugin slimScroll in dynamich divs"
description: "slimScroller"
keywords: ""
category: code
tags: [jquery, slimScroller]
---
{% include JB/setup %}

使用这个强大插件的同时，经常要和2个动态打交道：

1. 被slimScroll作用的DIV 可能是动态渲染的（ajax）
2. DIV 中内容的长度也是动态的，你可能需要一个动态的高度

插件的使用没得说，只强调一点，也是前端中最常见的一点，加载顺序问题。

如果如果DOM 是动态的，那么要保证加载完后还执行了`.slimScroll()`方法。

<!-- more -->
下面是一个封装好的方法：

``` javascript
//reInit Scroller after ajax html, especially for the latestNews Accordion
var reInitScroller = function(e){
    $('#'+e+' .scroller').each(function () {
        $(this).slimScroll({
                size: '7px',
                color: '#a1b2bd',
                position: 'right',
                height: $(this).attr("data-height"),//也可以设置为auto属性
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
    });
}

```

有时候设置了比如100px的滚动条区域，但是内容是动态的，有些只有1行，只占用了10px，下面很大空白。解决如下：

``` javascript
//如果内容少于300字，则不用滚动条化，直接全部显示出来；否则的话，用slimscroller化，设置其高度为100px
var isScroller = (item.content.length < 300) ? "" : "scroller";

string   += '<div style="font-size:12px;" class="' 
        + isScroller 
        + '" data-height="100px" data-always-visible="0" data-rail-visible1="1">' 
        + item.content 
        + '</div>';

reInitScroller();

```

slimScroller地址： [http://rocha.la/jQuery-slimScroll](http://rocha.la/jQuery-slimScroll)
