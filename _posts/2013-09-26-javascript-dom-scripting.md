---
layout: post
title: "javascript-dom-scripting"
keywords: "javascript dom"
description: "javascript&DOM编程思想"
category: code
tags: [javascript, book, getting-started]
---
{% include JB/setup %}
[JsDOM](https://github.com/phpgcs/jsdom)是根据经典JS入门教程《Javascript DOM 编程艺术》（2nd）的练习代码。 

<!-- more -->
Chapter4: 案例研究：一个javascript图片库
--

#### 4.1 几个DOM属性：
+ childNodes
+ nodeType
+ nodeValue
+ firstChild
+ lastChild

####4.2 如何利用DOM提供的方法去编写脚本
####4.3 如何利用事件处理函数把javascript脚本和网页集成在一起

Chapter5: 最佳实践：
-
####5.1 平稳退化
    考虑浏览器禁用js的情况（如爬虫），还能否正常访问你的网页
        <a href="http://www.example.com" onclick="popUp(this.href); return false;">Example</a>
####5.2 分离Javascript
    结构和样式分离（向CSS 学习
        getElementById(id).onclick = function(){};
####5.3 向后兼容
    对象检测
        if(!getElementById || !getElementByTagName) return false;
####5.4 性能考虑
+ 尽量少访问DOM和尽量少使用标记 var links =document.getElementsByTagName("a")
+ 合并和安置脚本,合并脚本,放在body之前
+ 压缩脚本l

Chapter6: 案例研究：图片库改进版
-
> 根据第5章内容，改进图片库l
####6.1 支持平稳退化
####6.2 分离Javascript和HTML
####6.3 向后兼容
####6.4 共享onload事件
####6.5 不要做太多假设
####6.6 键盘访问    
####6.7 DOM-core 和 HTML-DOM 
    eg. element.src / element.href /element.forms
