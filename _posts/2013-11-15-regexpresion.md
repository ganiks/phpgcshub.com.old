---
layout: post
title: "regexp"
tagline: "正则表达式入门"
keywords: "正则表达式"
description: ""
category: web
tags: [regexp, getting-started]
---
{% include JB/setup %}


<!-- more -->
用处：
==
1. 验证用户表单输入 用户名/密码/邮箱/身份证号码啊/电话号码
2. 发布文章,将输入有URL的地方都加上对应 的链接
3. 按有标点符号计算文章有多少个句子
4. 抓取网页中某种格式的数据

应用情景：
==
 1.perl,C#,Java,PHP
 2.B/S软件开发
 3.Linux操作系统
 4.Javascript脚本
 5.数据库Mysql

具体功能：
==
    制定规则, 对字符串进行比较,从而实现查找、替换、分割、匹配的操作

PHP中的正则：
==
PHP中 2种正则处理函数库
---
1. 由PCRE（Perl Compatibal Regular Expression）提供与perl语言兼容 的正则函数.以preg_开头
2. 由POSIX(Portable Operating System Interface)提供,以ereg_开头

常用的preg_函数
---
    preg_match()            匹配一个string,返回0/1
    preg_match_all()        全局匹配一个string,返回个数int
    preg_replace()            搜索并替换
    preg_split()            分割
    preg_grep()                对array进行匹配,并返回匹配的单元array
    preg_replace_callback() 用回调函数执行正则表达式的搜索和替换

hello world
---
``` php
    <?php
        $pattern = '/[a-zA-Z]+://[^/s]*/';
        $content = "lfdkjsalkjdsalj";
        if(preg_match($pattern, $content)
        {
            echo '匹配成功';
        }
    ?>
```

效率问题：
--
有时候简单的字符串操作请用PHP强大的字符串函数,instead of 正则 example：

`substr`、`strstr`、`stristr`、`strpos`、`strrpos`

`str_replace`、`str_ireplace`

`explode`、`implode`、`join`

语法规则
==

组成部分：
--

+ 定界符：    常用反斜线 / ,但是也可以用|| ## ！! {}
+ 原子：     普通字符/转义字符等 < a href = ' " / > \t \r \n
+ 元字符：     具有特殊含义的字符,如 [] () | . ? * +    
+ 模式修饰符：    s i m x e U D

原子：
--

1. 普通字符：a-z, A-Z, 0-9
2. 特殊字符 or 元字符：要转义哦 \. \* \< \> \+
3. 非打印字符：\f \n \r \t \v \cx(ctrl+x,其中x必须为a-zA-Z之一)
4. 通用字符类型：
    + \d \D 十进制数字          [0-9]
    + \s \S 空白字符            [\f\n\r\t\v]
    + \w \W 字母数字下划线      [0-9a-zA-Z_]
    + \D \S \W 代表它们的相反   
5. 自定义原子表[]：'/[apj]sp/' 匹配的是 asp php jsp 

元字符：
--

1. 限定符6个
    + *     0个/1个/多个    {0,}
    + +     一个/多个       {1,}
    + ？    0个/1个         {0,1}
    + {n}   n个
    + {n,}  至少n个
    + {n,m} 至少n个,至多m个
2. 模式选择符
    |       用来分割多选一模式,在正则表达式中匹配>=2个选择之一

3. 句号
    .       匹配任何一个字符,包括不可打印字符,但不匹配换行符
            相当于 [^\n]  Unix OS  or [^\r\n] Windows OS

边界限制
    + ^ \A    匹配输入字符串的开始位置（或者在多行模式下行的开头,即紧随一换行符之后）
    + $ \Z
    + \b        匹配单词的边界
    + \B        单词边界以外的部分
原子表
  []
  [^]

模式单元
  ()        匹配其整体为一个原子,一个模式单元中的表达式将优先匹配

后向引用
  \\1 \\2 \\3 ... \\99
  如： '/(?:Windows)(Linux)\\1OS/' -- LinuxOS  用?: ?= ?!来忽略对相关匹配的保存
      '/(Windows)(Linux)\\2OS/'     -- LinuxOS

模式匹配优先级：
    转义符号             \
    模式单元/原子表    () (?:) (?=) []
    重复匹配            * + ? {n} {n,} {n,m}
    边界限制            \b \B \A \Z ^ $


模式修正符
    + i    不区分大小写
    + m    (multiple lines)将字符串视为多行    /^is/m    匹配 this\nis\na\tes 可以匹配到 is（第二行开头的is）
    + s    (single line)圆点字符. 将匹配所有字符,包括换行符,即将字符串视为单行,换行符作为普通字符看待
    + x    空白忽略不计,除非已经转义的空白
    + e
    + U    匹配数量的值不再是默认的重复,在后面跟上？才是重复,/a.*?e/U 重复,/a.*e/U  /a.*?e/ 这两个不重复,不贪婪了
    + D

与perl兼容的正则表达式函数
==

匹配查找
``` php
int preg_match(string pattern, string subject,[, array matches])
    
    $pattern = '/(https?|ftps?):\/\/(www)\.([^\.\/]+)\.(com|net|org)(\/[\w-\.\/\?\%\&\=]*)?/i';
    $content = "网址为http://www.phpgcs.com/index.php的网站是PHP攻城师的网站";
    if(preg_match($pattern, $content, $matches))    {
        print_R($matches);
    }
    array(6)=>{
        '0'=>'http://www.phpgcs.com/index.php',
        '1'=>'http',
        '2'=>'www',
        '3'=>'phpgcs',
        '4'=>'com',
        '5'=>'/index.php'    
    }
```


``` php
int preg_match_all(string pattern, string subject, array matches[, predefined flags])

    预定义的flags 有2个,
默认是 PREG_PATTERN_OREDER     $matches[0]=>'全部模式匹配的数组', $matches[1]=>'第一个括号中的子模式所匹配的字符串组成的',... ...
还有是 PREG_SET_ORDER        $matches[0]=>'第一组匹配项的数组',$matches[1]=>'第2组匹配项的数组',...
其中 PREG_SET_ORDER 相当于是 preg_match 的复数形式
``` php
array preg_grep(string pattern, array input)
    匹配数组中的单元

mixed preg_replace(mixed pattern, mixed replacement, mixed subject[, int limit])
mixed str_replace (mixed search,  mixed replace,     mixed subject[,int &count])
array preg_split(string pattern, string subject[, int limit[, int flags]])
array explode    (string separator, string string[, int limit])
string implode    (string glue,        array pieces)
string join        (string glue,        array pieces)
    
    预定义的flags 有3个,可以是它们的组合,用按位|运算符组合
PREG_SPLIT_NO_EMPTY            返回非空成分
PREG_SPLIT_DELIM_CAPTURE    定界符模式中的括号表达式也会被返回
PREG_SPLIT_OFFSET_CAPTURE     对每个匹配的结果也同时返回其附属的字符串偏移量
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

看看常见的替代这几个函数的字符串操作函数



