---
layout: post
title: "jquery serialize serializeArray param 使用详解"
tagline: "jquery serialize serializeArray param 使用详解"
description: "jquery serialize/serializeArray/param 使用详解"
keywords: "jquery,serialize,serializeArray,param"
category: front-end 
tags: [jquery, bug]
---
{% include JB/setup %}


jquery 方法 $('form').serialize(); 
如果返回为空，检查下 form  的input 是否设置了 name 属性， 如果没有设置name属性，是返回空的

<!-- more -->

$('form').serialize();
==
The `.serialize()` method creates a text string in standard URL-encoded notation. 

1. 使用案例：`name=kdval 的input value=杭州` 经过 `serialize`之后 `kdval=%E6%9D%AD%E5%B7%9E&word=%E6%9D%AD%E5%B7%9E` 
2. 作用对象： `<input>, <textarea>,  <select>` 
3. 使用方法： `$( "input, textarea, select" ).serialize();`
4. 针对form:   jQuery serializes 该 form 中的 `successful controls`
5. 不要重复调用： Selecting both the form and its children in a set will cause duplicates in the serialized string.

6. `submit button`：不会被serialize, 因为 form 不会通过 button 提交的，可以通过input(type="submit") 或者 $.ajax
7. `name`属性: 必有name属性，才会被serialized
8. `checkboxes` and `radio buttons` : 只有`checked`，才会被serialized
9. `file` select elements: 不会被serialized



What are Successful controls
--
``` html 
17.13.2 Successful controls

A successful control is "valid" for submission. 

Every successful control has its control name paired with its current value as part of the submitted form data set. 

A successful control must be defined within a FORM element and must have a control name.

However:

Controls that are disabled cannot be successful.

If a form contains more than one submit button, only the activated submit button is successful.

All "on" checkboxes may be successful.

For radio buttons that share the same value of the name attribute, only the "on" radio button may be successful.

For menus, the control name is provided by a SELECT element and values are provided by OPTION elements.  Only selected options may be successful.  When no options are selected, the control is not successful and neither the name nor any values are submitted to the server when the form is submitted.

The current value of a file select is a list of one or more file names. 

Upon submission of the form, the contents of each file are submitted with the rest of the form data. The file contents are packaged according to the form's content type.
The current value of an object control is determined by the object's implementation.
If a control doesn't have a current value when the form is submitted, user agents are not required to treat it as a successful control.

Furthermore, user agents should not consider the following controls successful:

Reset buttons.
OBJECT elements whose declare attribute has been set.
Hidden controls and controls that are not rendered because of style sheet settings may still be successful. For example:

<FORM action="..." method="post">
<P>
<INPUT type="password" style="display:none"  
          name="invisible-password"
          value="mypassword">
</FORM>
will still cause a value to be paired with the name "invisible-password" and submitted with the form.

```

相关方法: $.param(); $.serializeArray();
==
$.param(obj);
--

Description: Create a serialized representation of an array or object, suitable for use in a URL query string or Ajax request.

``` javascript
//input
$.param($('form');
//output
=

//input
$.param($('input#kdval');
//output
=%E6%9D%AD%E5%B7%9E

//input
var params = { width:1680, height:1050 };
var str = jQuery.param( params );
//output
width=1680&height=1050

```
$.serializeArray();
--
Description: Encode a set of form elements as an array of names and values.

``` javascript

//input
$( "form" ).submit(function( event ) {
  console.log( $( this ).serializeArray() );
  event.preventDefault();
});

//output
[
  {
    name: "a",
    value: "1"
  },
  {
    name: "e",
    value: "5"
  }
]
```

$.serialize()  VS $.serializeArray()
==

这两个方法长得很像，一看就知道返回的类型不同，一个是序列化后的string， 一个则是object/array

```html
<form>
    <input name="kdval" value="hangzhou" />
    <input name="word" value="hangzhou" />
</form>

//input
var query = $('form').serialize();
console.log(query);

//output
kdval=%E6%9D%AD%E5%B7%9E&word=%E6%9D%AD%E5%B7%9E


//input
var query = $('form').serializeArray();
console.log(query);

//output
[Object, Object]

0: Object
name: "kdval"
value: "杭州"
__proto__: Object
1: Object
length: 2
__proto__: Array[0]
 
```

但是有意思的是，它们俩返回的结果在`$.ajax`方法中作为`data`属性的值是没有区别的。Why？

```javascript
        form_data = $(this).serializeArray();
        或者
        form_data = $(this).serialize();

        $.ajax({
            url: "/frontend_dev.php/coche1/update/id/1",
            type: "POST",
            data: form_data
            });
        });
```

参考：<a target="_blank" href="http://stackoverflow.com/questions/4235052/why-does-serialize-have-the-same-effect-as-serializearray-when-setting-post">
http://stackoverflow.com/questions/4235052/why-does-serialize-have-the-same-effect-as-serializearray-when-setting-post</a>

```javascript
If an object/array gets passed (which .serializeArray() returns), form_data serialized via $.param().

If a string get passed (which .serialize() returns)  form_data won't be processed any further.

...so they have the same effect when passed as the data property. You can find the relevant check here:

    // convert data if not already a string
    if ( s.data && s.processData && typeof s.data !== "string" ) {
        s.data = jQuery.param( s.data, s.traditional );
    }
```

