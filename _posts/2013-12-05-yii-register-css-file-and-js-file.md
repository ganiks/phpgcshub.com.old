---
layout: post
title: "yii register css file and js file"
description: "yii register css file and js file"
keywords: "yii, registerCssFile, registerScriptFile"
category: yii
tags: []
---
{% include JB/setup %}

<!-- more -->

``` php
<?php

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/media/css/blog.css');
//<link rel="stylesheet" type="text/css" href="/media/css/blog.css" />
//有效


Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/media/css/DT_bootstrap.css', "all");
//<link rel="stylesheet" type="text/css" href="/media/css/DT_bootstrap.css" media="all" />
//有效

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/media/css/DT_bootstrap.css', CClientScript::POS_END);
//<link rel="stylesheet" type="text/css" href="/media/css/DT_bootstrap.css" media="0" />
//无效

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/js/jquery.uniform.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/js/jquery.uniform.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/js/jquery.uniform.min.js', CClientScript::POS_END);

/**
 * Registers a piece of javascript code.
 * @param string $id ID that uniquely identifies this piece of JavaScript code
 * @param string $script the javascript code
 * @param integer $position the position of the JavaScript code. Valid values include the following:
 * <ul>
 * <li>CClientScript::POS_HEAD : the script is inserted in the head section right before the title element.</li>
 * <li>CClientScript::POS_BEGIN : the script is inserted at the beginning of the body section.</li>
 * <li>CClientScript::POS_END : the script is inserted at the end of the body section.</li>
 * <li>CClientScript::POS_LOAD : the script is inserted in the window.onload() function.</li>
 * <li>CClientScript::POS_READY : the script is inserted in the jQuery's ready function.</li>
 * </ul>
 * @param array $htmlOptions additional HTML attributes
 * Note: HTML attributes are not allowed for script positions "CClientScript::POS_LOAD" and "CClientScript::POS_READY".
 * @return CClientScript the CClientScript object itself (to support method chaining, available since version 1.1.5).
 */
public function registerScript($id,$script,$position=null,array $htmlOptions=array())
{
        if($position===null)
                $position=$this->defaultScriptPosition;
        $this->hasScripts=true;
        if(empty($htmlOptions))
                $scriptValue=$script;
        else
        {
                if($position==self::POS_LOAD || $position==self::POS_READY)
                        throw new CException(Yii::t('yii','Script HTML options are not allowed for "CClientScript::POS_LOAD" and "CClientScript::POS_READY".'));
                $scriptValue=$htmlOptions;
                $scriptValue['content']=$script;
        }
        $this->scripts[$position][$id]=$scriptValue;
        if($position===self::POS_READY || $position===self::POS_LOAD)
                $this->registerCoreScript('jquery');
        $params=func_get_args();
        $this->recordCachingAction('clientScript','registerScript',$params);
        return $this;
}

```
