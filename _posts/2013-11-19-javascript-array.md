---
layout: post
title: "javascript Array"
description: "javascript array"
keywords: ""
category: javascript
tags: [javascript]
---
{% include JB/setup %}

javascript 的数组操作无外乎拼接合并，删除添加，排序颠倒，挑来选去，跟字符串的转换;高级的用法多是以函数为参数的，map/some/filter/every/forEach/reduce...

<!-- more -->

基础方法
===

+ concat()      连接两个或更多的数组，并返回结果
+ join()        把数组的所有元素放入一个字符串,元素通过指定的分隔符进行分隔

<hr>
+ shift()       删除并返回数组的第一个元素
+ unshift()     向数组的开头添加一个或更多元素，并返回新的长度
+ pop()         删除并返回数组的最后一个元素
+ push()        向数组的末尾添加一个或更多元素，并返回新的长度
<hr>
+ reverse()     颠倒数组中元素的顺序
+ sort()        对数组的元素进行排序

<hr>
+ slice()       从某个已有的数组返回选定的元素
+ splice()      删除元素，并向数组添加新元素

+ //toSource()  返回该对象的源代码

<hr>
+ toString()    把数组转换为字符串，并返回结果
+ toLocaleString()    把数组转换为本地数组，并返回结果
+ valueOf()     返回数组对象的原始值

按照特点分类
===

Mutator methods (增变式）
---

These methods modify the array:

+ pop
+ push
+ reverse
+ shift
+ sort
+ splice
+ unshift

Accessor methods (访问式）
--

These methods do not modify the array and return some representation of the array.

+ concat
+ join
+ slice
+ toSource 
+ toString
+ indexOf 
+ lastIndexOf 

Iteration methods (迭代式）
--

+ forEach 
+ every 
+ some 
+ filter 
+ map 
+ reduce
+ reduceRight

Generic methods (类数组方法)
--

+ concat
+ join
+ pop
+ push
+ reverse
+ shift
+ slice
+ sort
+ splice
+ toSource
+ toString
+ unshift

//Introduced in JavaScript 1.6

+ every
+ filter
+ forEach
+ indexOf
+ lastIndexOf
+ map
+ some


Methods inherited from Object:
--
+ `__defineGetter__`
+ `__defineSetter__`
+ hasOwnProperty
+ isPrototypeOf
+ `__lookupGetter__`
+ `__lookupSetter__`
+ `__noSuchMethod__`
+ propertyIsEnumerable
+ toLocaleString
+ unwatch
+ valueOf
+ watch


敲代码练习下：
==

``` javascript

ar1=Array(1,2);
[1, 2]
ar1=Array(1,2),ar2=Array(3,4);
[3, 4]
ar12=ar1.concat(ar2);
[1, 2, 3, 4]
ar12=ar2.concat(ar1);
[3, 4, 1, 2]

ar12.join(',');
"3,4,1,2"
ar12.join('|');
"3|4|1|2"

ar12.pop();
2
ar12
[3, 4, 1]
//跟参数无关
ar12.pop(2);
1
ar12
[3, 4]
ar = [1,2,3,4,5];
[1, 2, 3, 4, 5]
typeof(ar);
"object"
typeof(ar12);
"object"
3

ar.length();
TypeError: Property 'length' of object [object Array] is not a function
ar.length;
5

ar.push(6,7,8);
8

ar.reverse();
[8, 7, 6, 5, 4, 3, 2, 1]
ar.reverse();
[1, 2, 3, 4, 5, 6, 7, 8]

//跟参数无关
ar.shift();
1
ar.shift(2);
2
ar.shift(2);
3

ar.unshift(1,2,3);
8
ar;
[1, 2, 3, 4, 5, 6, 7, 8]
ar;
[1, 2, 3, 4, 5, 6, 7, 8]

ar;
[4, 5, 6, 7, 8]
ar.slice(0,2);
[4, 5]
ar;
[4, 5, 6, 7, 8]
ar.slice(3,2);
[]
ar.slice(2,2);
[]
ar.slice(1,2);
[5]
ar.slice(0,2);
[4, 5]
ar;
[4, 5, 6, 7, 8]
6

ar.splice(1,2);
[2, 3]
ar;
[1, 4, 5, 6, 7, 8]
3
ar.splice(1,0,2,3);
[]
ar;
[1, 2, 3, 4, 5, 6, 7, 8]
ar.splice(8,0,9,10);
ar;
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
ar.splice(10,2,11,12);
[]
ar;
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]


ar=["a1","c1","b1"]
["a1", "c1", "b1"]
ar.sort();
["a1", "b1", "c1"]
ar=[11,3,,8,4];
[11, 3, undefined × 1, 8, 4]
ar.sort();
[11, 3, 4, 8, undefined × 1]
ar.sort(function(a,b){return a-b;});
[3, 4, 8, 11, undefined × 1]

ar=[1,2,3];
[1, 2, 3]
ar.toString();
"1,2,3"
ar=[1,2,3];
[1, 2, 3]
ar.toLocaleString();
"1,2,3"
3
ar.valueOf();
[1, 2, 3]
ar;
[1, 2, 3]

```

高级点的方法
==

``` javascript

function isBiggerThan10(elem,index,array){ 
    return (elem>=10);
};
isBiggerThan10(4);
false
isBiggerThan10(14);
true
[12,13,4].every(isBiggerThan10);
false
[12,13,14].every(isBiggerThan10);
true
[2,3,14].some(isBiggerThan10);
true
[2,3,4].some(isBiggerThan10);
false
[2,3,14].filter(isBiggerThan10);
[14]
[2,3,4].filter(isBiggerThan10);
[]

function logArrayElements(element, index, array) {
    console.log("a[" + index + "] = " + element);
}
["Tom","John"].forEach(logArrayElements);
a[0] = Tom
a[1] = John

function pulsAs(elem, index, array){ return elem.replace(/o/g, 'e');};
["foo", "foot", "moon"].map(pulsAs);
["fee", "feet", "meen"]

ar=[0,1,2,3];
[0, 1, 2, 3]
ar.reduce(function(pre,cur,index,array){return pre+cur; });
6
ar.reduce(function(pre,cur,index,array){return pre+cur; }, 10);
16
ar.reduceRight(function(pre,cur,index,array){return pre+cur; });
6
ar.reduceRight(function(pre,cur,index,array){return pre+cur; },10);
16

ar=["cat","love","fish"];
["cat", "love", "fish"]
ar.reduce(function(pre,cur){return pre+" "+cur; });
"cat love fish"
ar.reduce(function(pre,cur){return pre+" "+cur; }, 10);
"10 cat love fish"
ar.reduceRight(function(pre,cur){return pre+" "+cur; });
"fish love cat"
ar.reduceRight(function(pre,cur){return pre+" "+cur; }, 10);
"10 fish love cat"

```
