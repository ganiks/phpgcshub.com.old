---
layout: post
title: "yii-user-management extension"
description: "yii-user-management extension"
keywords: "yii-user-management extension"
category: yii
tags: []
---
{% include JB/setup %}

<!-- more -->

yii-user-management
===================

a user management module collection for the yii framework

Features:

* 自动安装
* 用户管理
* 角色管理
* 整合Hybrid
* 整合了RBAC和ACL的权限系统
* 消息系统(子模块)
* 用户组
* 用户Avatar头像上传
* 用户注册
* 密码找回
* 朋友系统
* 邮件component
* 基本语言：English，已经完成了德语法语的翻译

Get source:
====================================
git clone git@github.com:thyseus/yii-user-management.git

Install:
====================================

参考官方文档：
<a href="https://code.google.com/p/yii-user-management/wiki/InstallationInstructions" target="_blank">https://code.google.com/p/yii-user-management/wiki/InstallationInstructions</a>

源码中是7个模块的文件夹，放到`/protected/modules/`目录下面

1,核心模块是 user ，注册这个模块以执行里面的Intall方法
----------
`/config/main.php`

```php 
'modules'=>array(
    'user'=>array(
        'debug'=>true,
    ),
),
``` 

其他模块可以在安装完YUM（yii-user-management)之后再注册

2,配置数据库
----------

```php 
'db'=>array(
    //db config
   'tablePrefix'=>'yum',
)
```

especially the tablePrefix or you'll get error like this:

CDbCommand 无法执行 SQL 语句: SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '{translation}} where language = 'zh_cn' and category = 'yum'' at line 1. The SQL statement executed was: select message, translation from {{translation}} where language = :language and category = :category '

because yum uses the {{table}} syntax extensively and yii seems to behave awkward without that setting.

3，配置cache component
----------

```php
'cache' => array(
    'class' => 'system.caching.CDummyCache',
),
```

4,Make your project use the YUM authentication methods
----------

```php
<?
'components'=>array(
 'user'=>array(
      'class' => 'application.modules.user.components.YumWebUser',
      'allowAutoLogin'=>true,
      'loginUrl' => array('//user/user/login'),
      [...]
    ),

 'import'=>array(  
  'application.modules.user.models.*',
        [...]
```

Now, your can run `http://yourapp/user/install`

当安装成功结束后，会给出配置实例代码：

```bash
'modules'=>array(
    'user' => array(
        'debug' => false,
        'userTable' => 'yum_user',
        'translationTable' => 'yum_translation',
    ),
    'usergroup' => array(
        'usergroupTable' => 'yum_usergroup',
        'usergroupMessageTable' => 'yum_user_group_message',
    ),
    'membership' => array(
        'membershipTable' => 'yum_membership',
        'paymentTable' => 'yum_payment',
    ),
    'friendship' => array(
        'friendshipTable' => 'yum_friendship',
    ),
    'profile' => array(
        'privacySettingTable' => 'yum_privacysetting',
        'profileTable' => 'yum_profile',
        'profileCommentTable' => 'yum_profile_comment',
        'profileVisitTable' => 'yum_profile_visit',
    ),
    'role' => array(
        'roleTable' => 'yum_role',
        'userRoleTable' => 'yum_user_role',
        'actionTable' => 'yum_action',
        'permissionTable' => 'yum_permission',
    ),
    'message' => array(
        'messageTable' => 'yum_message',
    ),
),  
```

Configuration of your freshly installed User Management Module
=====
Language
-------------------
The Yii-User Management Module uses the language that is set in the Application Configuration. For example, you can add a

`'language' => 'de',`

in your `config/main.php` to get German Language strings. 

At the moment English, German, Spanish, French and Polish are supported.

Quick Login Widget
-----------------
If you want to display a quick login widget somewhere in your Web Application, just call in your view file:

``` php
<?php $this->widget('application.modules.user.components.LoginWidget'); ?>
```

Password Requirements
--------------------
You can setup the password Requirements within the 'passwordRequirements' option of the Module, for example:

``` php
 'user' => array(
        'passwordRequirements' => array(
          'minLen' => 4,
          'maxLen' => 16,
          'maxRepetition' => 2,
          'minDigits' => 3,
          ),
```

更多 密码配置查看`components/CPasswordValidator.php`

User Registration
-------------------------
Set the Variable `disableEmailActivation` in the module configuration to let users be able to register to your Application without Registration.

Role Management
------------------------------------------
You can add up new roles in the Role Manager. 

To check for access to this roles, you can use this code Snippet everywhere in your Yii Application. 

Most likely it will be used in the ACL Filter of your Controllers:

```php
if(Yii::app()->user->can('action'))
{
 // user is allowed
}
else
{
 // user is not allowed to do this
}
```

Please see the file `docs/logging.txt` for information on how to set up the logging functions of the Yii User Management module.

Where to go from now on?
There are some examples on how to extend from the Yii User Management Module and how to implement project-specific stuff. See the files in the docs/ directory for all this.


custom
======

禁用YUM的样式

禁用 `bootstrap.min.css` :

`user/UserModule.php`

public $enableBootstrap = false;

禁用 `yum.css`

vi user/views/layouts/yum.php 

``` php
<?php
//Yii::app()->clientScript->registerCssFile(
//› ›   Yii::app()->getAssetManager()->publish(
//› ›   ›   Yii::getPathOfAlias('YumModule.assets.css').'/yum.css'));
```

