---
layout: post
title: "highly scalable echarts map"
tagline: "高度可扩展的Echarts地图（中国地图控件）"
description: ""
keywords: ""
category: Data Visualization
tags: [echarts]
---
{% include JB/setup %}

Echarts的map有以下特性:

+ 同时展示全国和省份的地图
+ 支持多个系列数据
+ 地图样式的高度可扩展性

<!-- more -->
本文实现目标：
--
+ 省市联动显示正面/中性/负面3系列的数据
+ 不同系列数据展示时地图的颜色变化
+ 初始默认选中河南省

Map使用总结
==

省市地图的同时联动展示
--
通过参数`option.series[x].mapType`数`option.series[x].mapLocation`来分别确定地图类型和地图位置。

比如常见的是国家图在左侧，省图右侧，一共展示3系列数据：

+ data.series[0]~data.series[5] 一共有6组数据（2种图*3系列）
+ data.series[x].mapType = 'china' and '河南'
+ data.series[x].mapLocation: {x: 'left'} and {x: 500}

地图颜色配置和值域漫游
--
+ 地图颜色，是由`dataRange`的color:[最深色， 最浅色]渐变的
+ 动态颜色可以用`dataRange`来实现
+ 值域漫游由 `calculable: true,`开启，如果数据量过大，会很卡的。

扩展行为有2种方式
---
+ 动态的option(看option.dataRange.color 的动态实现)
+ 用chart实例的`on`方法来监听事件,如`LEGEND_SELECTED` `MAP_SELECTED`

option 中动态方法的实现
--
`function(){ ...; return sth.; }()` 

根据legend选择动态改变地图的背景色
--
背景色是由`dataRange`决定的，所以用一个全局变量`legendmode`来记录legend 当前的状态，结合自执行函数来返回相应的颜色。

当然，`legendmode`是由监听`LEGEND_SELECTED` 得到的。

切换省份后legend状态的保持
--
为了保证用户切换省份时也能保持legend的状态，需要使用动态的option.legend

``` javascript 
        legend : {
            x:'right',
            data:['正面', '负面', '中性'],
            selected: legendmode,
        },
``` 

下面是完整的源码：
---

``` javascript
var legendmode = {};
var get_weiboprovince_option = function (data, selectedProvince){
    var option = {
        tooltip : {
            trigger: 'item'
        },
        toolbox: {
            show : true,
            orient: 'vertical',
            x:'right',
            y:'center',
            feature : {
                restore : true,
                saveAsImage : true
            }
        },
        dataRange: {
            orient: 'horizontal',
            x: 'left',
            y: 'bottom',
            min: 0,
            max: 6000,
            text:['高','低'],
            splitNumber:0,
            color: function () {
                if(!legendmode['正面'] && !legendmode['负面'] && !legendmode['中性']){
                    return ['#363636','#9c9c9c'];
                }
                if(!legendmode['正面'] && !legendmode['负面']){
                    return ['#1874CD', '#87CEFF'];
                }
                if(!legendmode['负面'] && !legendmode['中性']){
                    return  ['#CD6839', '#DEB887'];
                }
                if(!legendmode['正面'] && !legendmode['中性']){
                    return ['#9932CC', '#EE82EE'];
                }
                return ['#363636','#9c9c9c'];
            }(),
        },
        legend : {
            x:'right',
            data:['正面', '负面', '中性'],
            selected: legendmode,
        },
        series : [
            {
                name: '正面',
                type: 'map',
                mapType: 'china',
                mapLocation: {
                    x: 'left',
                },
                selectedMode : 'single',
                itemStyle:{
                    normal:{label:{show:true,textStyle:{color:'#111'}}, areaStyle:{color:'#ff0000'}},
                    emphasis:{label:{show:true}}
                },
                data: function () {
                    var list = [];
                    for(x in data['province']) {
                        if(x == selectedProvince) {
                            list.push('{name:"'+ x +'", selected:true, value:'+ data['province'][x]['zm'] +'}');
                        } else {
                            list.push('{name:"'+ x +'", selected:false, value:'+ data['province'][x]['zm'] +'}');
                        }
                    }
                    return eval("[" + list.toString() + "]");
                }()
            },
            {
                name: '负面',
                type: 'map',
                mapType: 'china',
                mapLocation: {
                    x: 'left',
                },
                selectedMode : 'single',
                itemStyle:{
                    normal:{label:{show:true,textStyle:{color:'#111'}}, areaStyle:{color:'#ff0000'}},
                    emphasis:{label:{show:true}}
                },
                data: function () {
                    var list = [];
                    for(x in data['province']) {
                        if(x == selectedProvince) {
                            list.push('{name:"'+ x +'", selected:true, value:'+ data['province'][x]['zx'] +'}');
                        } else {
                            list.push('{name:"'+ x +'", selected:false, value:'+ data['province'][x]['zx'] +'}');
                        }
                    }
                    return eval("[" + list.toString() + "]");
                }()
            },
            {
                name: '中性',
                type: 'map',
                mapType: 'china',
                mapLocation: {
                    x: 'left',
                },
                selectedMode : 'single',
                itemStyle:{
                    normal:{label:{show:true,textStyle:{color:'#111'}}, areaStyle:{color:'#ff0000'}},
                    emphasis:{label:{show:true}}
                },
                data: function () {
                    var list = [];
                    for(x in data['province']) {
                        if(x == selectedProvince) {
                            list.push('{name:"'+ x +'", selected:true, value:'+ data['province'][x]['fm'] +'}');
                        } else {
                            list.push('{name:"'+ x +'", selected:false, value:'+ data['province'][x]['fm'] +'}');
                        }
                    }
                    return eval("[" + list.toString() + "]");
                }()
            },
            {
                name: '正面',
                type: 'map',
                mapType: selectedProvince,
                mapLocation: {
                    x: 500
                },
                itemStyle:{
                    normal:{label:{show:true,textStyle:{color:'#111'}}, areaStyle:{color:'#ff0000'}},
                    emphasis:{label:{show:true}}
                },
                data: function () {
                    var list = [];
                    for(var x in data['city']) {
                        list.push('{name:"'+ x +'", selected:false, value:'+ data['city'][x]['zm'] +'}');
                    }
                    return eval("[" + list.toString() + "]");
                }()
            },
            {
                name: '负面',
                type: 'map',
                mapType: selectedProvince,
                mapLocation: {
                    x: 500
                },
                itemStyle:{
                    normal:{label:{show:true,textStyle:{color:'#111'}}, areaStyle:{color:'#ff0000'}},
                    emphasis:{label:{show:true}}
                },
                data: function () {
                    var list = [];
                    for(var x in data['city']) {
                        list.push('{name:"'+ x +'", selected:false, value:'+ data['city'][x]['zx'] +'}');
                    }
                    return eval("[" + list.toString() + "]");
                }()
            },
            {
                name: '中性',
                type: 'map',
                mapType: selectedProvince,
                mapLocation: {
                    x: 500
                },
                itemStyle:{
                    normal:{label:{show:true,textStyle:{color:'#111'}}, areaStyle:{color:'#ff0000'}},
                    emphasis:{label:{show:true}}
                },
                data: function () {
                    var list = [];
                    for(var x in data['city']) {
                        list.push('{name:"'+ x +'", selected:false, value:'+ data['city'][x]['fm'] +'}');
                    }
                    return eval("[" + list.toString() + "]");
                }()
            },
        ],
        animation: false
    };
    return option;
}
var drawing_weiboprovince = function (data) {
    var weiboprovince_chart = myMap.init(document.getElementById('weiboprovince_chart'));
    var ecConfig = require('echarts/config');
    var selectedProvince = '河南';
    weiboprovince_chart.on(ecConfig.EVENT.LEGEND_SELECTED, function(param){
        legendmode = param.selected;
        option = get_weiboprovince_option(data, selectedProvince);
        if(!legendmode['正面']){
            option.series.splice(0,1,{});
            option.series.splice(3,1,{});
        }
        if(!legendmode['河南']){
            option.series.splice(1,1,{});
            option.series.splice(4,1,{});
        }
        if(!legendmode['负面']){
            option.series.splice(2,1,{});
            option.series.splice(5,1,{});
        }
        weiboprovince_chart.setOption(option, true);
    });
    weiboprovince_chart.on(ecConfig.EVENT.MAP_SELECTED, function(param){
        var selected = param.selected;
        for (var name in selected) {
            if (selected[name]) {
                selectedProvince = name;
            }
        }
        option = get_weiboprovince_option(data, selectedProvince);
        if (typeof selectedProvince == 'undefined') {
            option.series.splice(0);
            option.legend = null;
            option.dataRange = null;
            weiboprovince_chart.setOption(option, true);
            return;
        }
        weiboprovince_chart.setOption(option, true);
    });
    weiboprovince_chart.setOption(get_weiboprovince_option(data, '河南'), true);
};

```

