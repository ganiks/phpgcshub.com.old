---
layout: post
title: "听&看"
tagline: "listen and watch"
keywords: "jianshu,简书,teahour,听和看"
description: "jianshu,简书,teahour,听和看"
category: think
tags: [listen, imbark]
---
{% include JB/setup %}

早上浦东12路公交车上的20～30分钟都要做些什么，这是个问题。

<!-- more -->
刚搬来的时候，在手机上看视频

后来热衷玩游戏，如节奏大师，狂野飚车

其实更多的时候是听歌，看着车窗外拥挤着上车的人群

> 把青春留给身后那座辉煌的都市
>
> 为了这个美梦我们付出这代价
>
> --<私奔>

最近，喜欢上了自媒体《罗辑思维》老罗媒体早上发的60秒音频

喜欢上了[teahour.fm](http://teahour.fm)的互联网方面的人物访谈

<hr>

听的时候，我的眼睛是闲着的，但是精力却比看的时候更加的集中了

更重要的是，我发现我开始思考……

今天听的这一期teahour很感兴趣，因为我对[简书](http://jianshu.io)的产品很感兴趣

[http://teahour.fm/2013/09/16/interview-with-zhao-bo-about-jianshu.html](http://teahour.fm/2013/09/16/interview-with-zhao-bo-about-jianshu.html)

<audio src="http://screencasts.b0.upaiyun.com/podcasts/teahour_episode_32.m4a"  controls="controls">
    [http://teahour.fm/2013/09/16/interview-with-zhao-bo-about-jianshu.html](http://teahour.fm/2013/09/16/interview-with-zhao-bo-about-jianshu.html)
</audio>

> Show Notes:
>
> 本期由 Daniel Lv 和 Terry Tai 共同主持，邀请到了简书的联合创始人，赵波 来跟大聊聊他从大学毕业加入 SAP 公司的工作经历，分享了他对 SAP 这家公司和 ERP 的理解，以及之后从大公司离职，跟朋友一起创业，创立简书的故事。除此之外，赵波还跟分享了大学生如何规划自己的职业，毕业生如何求职，进入大公司要做那些准备，在大公司的工作经历可以给你带来什么？以及在校大学生如何争取一个互联网创业公司的实习机会等等。


tips:
---

1. 关于互联网开发入行

    更重要的是对规范和协议的理解，因为所有的web app都是构建在各种规范之上的

    相比较根据guide快速开发出一个应用，对web原理的理解要重要的多

    你要有真正想要做一名web程序员的desire，eager

2. 关于产品和经营

    不会去一定要追求怎样怎样，前提是保证产品走下去
    
    赵波他们现在是用做外包的钱来支撑着简书产品，说到平衡产品和外包之间的关系，赵波的观点是要以保证产品能走下去为前提。

    不是说为了要赶产品就加班到深夜2点

3. 用户的鼓励是支撑走下去的真正动力
    

