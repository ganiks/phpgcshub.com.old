---
layout: post
title: "teahour 10th episode: open source and gitcafe"
description: ""
keywords: ""
category: think 
tags: [teahour]
---
{% include JB/setup %}

本期由Kevin Wang主持, 邀请到上海Linux User Group 的负责人和GitCafe的创始人 Thomas Yao 来聊聊开源软件的话题和GitCafe 的创业经历。

<!-- more -->
+ Thomas最喜欢的两个发行版
    + Debian 简洁
    + Gentoo 基于LFS
+ 开源软件 VS 自由软件
    + Richard Stallman - gnu - free as freedom
    + Linus Torvalds - Linux
    + Eric S Raymond - promotion for open-source career
        + 《大教堂和大集市》
        + 如何成为一名黑客
        + Unix编程艺术
    + a movie 《Revolution OS》
+ GPL VS MIT
+ 商业公司是如何权衡开源和闭源的
    + Taobao Nginx
    + 半开半闭

    Skinkybad
    500px
    Fancy
    Overapi
    Martin Fowler
    Ruby Rogue's Book Club with Martin Fowler
    Alfred App
