---
layout: post
title: "use echarts"
tagline: "with json and javascript"
description: ""
keywords: ""
category: code 
tags: [echarts]
---
{% include JB/setup %}

<!-- more -->
``` javascript

var sourcePie, sourcePieOption, sourcePieChart = document.getElementById('sourcePie');
var line, lineOption={}, lineChart = {
    appraiseLine: document.getElementById('appraiseLine'),
    popularSourceLine: document.getElementById('popularSourceLine'),
};
var sentimentPie, sentimentPieOption={}, sentimentPieChart = {
    all: document.getElementById('sentimentPie_all'),
    news: document.getElementById('sentimentPie_news'),
    weibo: document.getElementById('sentimentPie_weibo'),
};

require.config({
    packages: [
        {
            name: 'echarts',
            location: '../../../../media/echarts/src',
            main: 'echarts'
        },
        {
            name: 'zrender',
            //location: 'http://ecomfe.github.io/zrender/src',
            location: '../../../../media/zrender/src',
            main: 'zrender'
        }
    ]
});

function requireConfig() {
    // ¿?¿?¿?¿?
    require(
        [
            'echarts',
            'echarts/chart/line',
            'echarts/chart/bar',
            'echarts/chart/pie',
        ],
        requireCallback
    );
}
function requireForTab() {
    // ¿?¿?¿?¿?
    require(
        [
            'echarts',
            'echarts/chart/pie',
        ],
        requireCallbackForTab
    );
}
function requireCallbackForTab (echarts){
    sentimentPie = {
        all: echarts.init(sentimentPieChart.all),
        news: echarts.init(sentimentPieChart.news),
        weibo: echarts.init(sentimentPieChart.weibo),
    };
}
function requireCallback (echarts) {
    sourcePie = echarts.init(sourcePieChart);
    requireCallbackForTab(echarts);
    line = {
        appraiseLine: echarts.init(lineChart.appraiseLine),
        popularSourceLine: echarts.init(lineChart.popularSourceLine),
    }
}
requireConfig();

//reInit Scroller after ajax html, especially for the latestNews Accordion
var reInitScroller = function(e){
    $('#'+e+' .scroller').each(function () {
        $(this).slimScroll({
                size: '7px',
                color: '#a1b2bd',
                position: 'right',
                height: $(this).attr("data-height"),
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
    });
}

$(function() {

    var mediaFocus = $('#mediaFocus_url').attr('data-url').split('|');
    var mediaFocus_Url = mediaFocus[0];
    var mediaFocus_Kdval = mediaFocus[1];
    var mediaFocus_Table = mediaFocus[2];
    var checkValue = mediaFocus_Kdval;
    var checkText = '';
    callbackSelected(checkValue);


    $('#config_front_kdval span').bind('click',function(){
        checkText = $(this).html();
        checkValue = $(this).attr('val');
        updateIsFrontFlag_Url = $(this).attr('url');
        //reload the Index Page
        callbackSelected(checkValue);
        //modify the 'isFront' flag in DB
        updateIsFrontFlag(updateIsFrontFlag_Url);
    });

    function callbackSelected(checkValue) {
        loadMediaFocus(checkValue);
        loadLatestNews(checkValue);
        loadSourcePie(checkValue);
        loadSentimentPie(checkValue);
        loadLine(checkValue, "appraiseLine");
        loadLine(checkValue, "popularSourceLine");
    }

    function updateIsFrontFlag(url) {
    }

    function loadSentimentPie(kdval){
        var pie = $('#sentimentPie_url').attr('data-url').split('|');
        var pie_url = pie[0];
        $.getJSON(pie_url, {"kdval": kdval}, function(data){
            loadSentimentPieLoop(data, 'all');
            loadSentimentPieLoop(data, 'news');
            loadSentimentPieLoop(data, 'weibo');
        });
    };

    var colorArray = [
        'rgba(138, 43, 226, 0.5)',
        'rgba(30, 144, 255, 0.6)',
        'rgba(255, 69, 0, 0.7)',
        'rgba(155, 69, 0, 0.7)',
        'rgba(155, 169, 0, 0.7)',
    ];

    function loadLine(kdval, type){
        var line_url = $('#'+type+'_url').attr('data-url').split('|');
        var line_url = line_url[0];
        $.getJSON(line_url, {"kdval": kdval}, function(data){
            var lineData = {"xline":[], "yline":[], "series":[]};
            var i =0, j=0;
            for(var y in data.series){
                    lineData.yline[j] = y;
                for(var x in data.series[y]){
                    if(y == lineData.yline[0]) lineData.xline[i] = x;
                    lineData.series[j] = {
                            name: y,
                            type:'line',
                            stack: '¿?¿?',
                            itemStyle: {normal: {areaStyle: {
                                color: colorArray[j]
                            }}},
                            data: json2array(data.series[y])
                        },
                    i++;
                }
                i = 0;
                j++;
            }
            loadLineOption(lineData, type);
        });
    };

    function loadLineOption(lineData, type){
        lineOption = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:lineData.yline
            },
            toolbox: {
                show : true,
                feature : {
                    magicType:['line', 'bar'],
                    restore : true,
                    saveAsImage : true
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 0,
                end : 70
            },
            grid: [
                {
                    width: 700,
                    height: 400,
                }
            ],
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : lineData.xline
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : lineData.series,
        };
        line[type].setOption(lineOption, true);
    }

    function loadSentimentPieLoop(data, type){
            sentimentPieOption[type] ={
                title : {
                    text: '¿?¿?¿?¿?¿?',
                    subtext: '',
                    x:'center'
                },
                tooltip : {
                    show : true,
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['¿?¿?','¿?¿?', '¿?¿?']
                },
                toolbox: {
                    show : true,
                    feature : {
                        restore : true,
                        saveAsImage : true
                    }
                },
                calculable : true,
                series : [
                    {
                        name:'¿?¿?',
                        type:'pie',
                        radius : [0, 120],
                        center: [,185],
                        itemStyle : {
                            normal : {
                                label : {
                                    position : 'inner',
                                    formatter : function(a,b,c,d) {return b + ' ' + (d - 0).toFixed(0) + ' %'},
                                    textStyle : {
                                        color : 'white',
                                        align : 'center',
                                        baseline : 'middle',
                                        fontFamily : '¿?¿?¿?¿?',
                                        fontSize : 16,
                                        fontWeight : 'bolder'
                                    }
                                },
                                labelLine : {
                                    show : false
                                }
                            }
                        },
                        data:[
                            {value:data.series[type].zm, name:'¿?¿?'},
                            {value:data.series[type].zx, name:'¿?¿?'},
                            {value:data.series[type].fm, name:'¿?¿?'},
                        ]
                    }
                ]
            };
            sentimentPie[type].setOption(sentimentPieOption[type], true);
    }

    //specially re-init and render echarts for bootstrap-tab plugin
    $('a[id^="sentimentTab_"]').on('shown', function(e){
        requireForTab();
        sentimentPie[$(this).attr('name')].setOption(sentimentPieOption[$(this).attr('name')], true);
    });

    function loadSourcePie(kdval){
        var pie = $('#sourcePie_url').attr('data-url').split('|');
        var sourcePie_url = pie[0];
        $.getJSON(sourcePie_url, {"kdval": kdval}, function(data){
            sourcePieOption ={
                title : {
                    text: '¿?¿?¿?¿?¿?¿?¿?',
                    subtext: '',
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['¿?¿?','¿?¿?']
                },
                toolbox: {
                    show : true,
                    feature : {
                        restore : true,
                        saveAsImage : true
                    }
                },
                calculable : true,
                series : [
                    {
                        name:'¿?¿?',
                        type:'pie',
                        radius : [80, 130],
                        center: [,225],
                        data:[
                            {value:data.series.sourceType.news, name:'¿?¿?'},
                            {value:data.series.sourceType.weibo, name:'¿?¿?'},
                        ]
                    }
                ]
            };
            sourcePie.setOption(sourcePieOption, true);
            });
    };

    function loadMediaFocus(kdval){
        $.getJSON(mediaFocus_Url, {"kdval": kdval}, function(data){
            tarray = data.series;
            var tbody = "";
            for(var x in tarray)
            {
                tbody += "<tr>";
                tbody += "<td>" + (parseInt(x)+1) +"</td>";
                tbody += "<td>" + "<a href=\"" + mediaFocus_Url + "\">" + tarray[x]['name'] + "</a>" + "</td>";
                tbody += "<td>" + tarray[x]['value'] + "</td>";
                tbody += "<td>" + tarray[x]['perc'] + "%" + "</td>";
                tbody += "</tr>";
            }
            $('#'+mediaFocus_Table).html(tbody);
        });
    };

    function loadLatestNews(kdval){
        var latestNews = $('#latestNews_url').attr('data-url').split('|');
        var latestNews_Url = latestNews[0];
        var latestNews_Kdval = latestNews[1];
        var latestNews_News = latestNews[2];
        var latestNews_Weibo = latestNews[3];

        var newsItemHeader = "<div class=\"accordion-group\"> <div class=\"accordion-heading\"> <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" ";
        var newsItemMiddle = " <div class=\"accordion-inner\"> <div class=\"scroller\" data-height=\"120px\" data-always-visible=\"1\" data-rail-visible=\"0\">";
        var newsItemFooter = "</div> </div> </div> </div>";
        var newsString = "<div class=\"accordion\" id=\"accordion1\" style=\"height: auto;\">";

        var weiboString = "";

        $.getJSON(latestNews_Url, {"kdval": kdval}, function(data){
            var newsArray = data.series.news;
            var weiboArray = data.series.weibo;
            for(var x in newsArray)
            {
                newsString += newsItemHeader;
                newsString += " href=\"#collapse_1_1_" + (parseInt(x)+1) + "\">";
                newsString += newsArray[x].title;
                newsString += (parseInt(x) === 0) ? "</a> </div> <div id=\"collapse_1_1_" + (parseInt(x)+1) + "\" class=\"accordion-body collapse in\"> " : "</a> </div> <div id=\"collapse_1_1_" + (parseInt(x)+1) + "\" class=\"accordion-body collapse\"> ";
                newsString += newsItemMiddle;
                newsString += nl2br(newsArray[x].content);
                newsString += newsItemFooter;
            }
            newsString += "</div>";
           $('#'+latestNews_News).html(newsString);
            reInitScroller(latestNews_News);

            for(var y in weiboArray)
            {
                weiboString += ' <li class="in"> <img class="avatar" alt="" src="media/image/avatar1.jpg" /> <div class="message"> <span class="arrow"></span> <span style="color:green">' + weiboArray[y].uname + '</span> ¿?¿?¿?  <span class="datetime">' + weiboArray[y].pubtime + '</span> <span class="body">'
             + weiboArray[y].content
             + '&nbsp;<span class="label label-info">¿?¿?¿?'
             + weiboArray[y].sentiment
             +'</span>&nbsp;'
             + '<span class="label label-alert">¿?¿?¿?'
             + weiboArray[y].reposts_count
             +'</span>&nbsp;'
             + '<span class="label label-success">¿?¿?¿?'
             + weiboArray[y].comments_count
             +'</span>'
             + '</span> </div> </li> ';
            }
            $('#'+latestNews_Weibo+' ul.chats').html(weiboString);
        });

    }

    function nl2br (str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }

    function json2array(json){
        var result = [];
        var keys = Object.keys(json);
        keys.forEach(function(key){
            result.push(json[key]);
        });
        return result;
    }
});



```
