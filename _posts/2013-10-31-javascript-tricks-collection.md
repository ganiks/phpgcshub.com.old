---
layout: post
title: "javascript json/array/string tricks collection"
description: ""
keywords: "JavaScript, json, array, string"
category: code
tags: [javascript]
---
{% include JB/setup %}

<!-- more -->
元素为对象的数组～字符串
==
``` javascript
var a = [{"obj1":"phpgcs"}, {"obj2":"ganiks"}]
console.log(a);
//[Object, Object]
JSON.stringify(a)
//"[{"obj1":"phpgcs"},{"obj2":"ganiks"}]"
```

获取URL中参数
==
``` javascript
//extend JQuery
(function($){
    $.getUrlParam = function(name)
    {
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r!=null) return decodeURI(r[2]); return null;
    }
})(jQuery);

//use
$('#keyword').val($.getUrlParam('keyword'));

//or simply in one centence
param = decodeURI((RegExp('kdval' + '=' + '(.+?)(&|$)').exec(url)||[,null])[1]);

```
> attention : `decodeURI()` is a must to decode the params, otherwise it'll be wrong-encoding

javascript解码
==
decodeURI() 函数可对 encodeURI() 函数编码过的 URI 进行解码。

javascript 拼接HTML技巧
==
用单引号而不是双引号，这样很多地方的双引号就不用转义了

``` javascript 
 string += '<blockquote>'
        + '<span style="color:green">'
        + item.uname
        + '</span>&nbsp;发表于 '
        + '</blockquote>';
```

jQuery(function(){})与(function(){})(jQuery)的区别
==

`jQuery(function(){ });` 全写为 `jQuery(document).ready(function(){ });` 意义为在DOM加载完毕后执行了ready()方法。

jQuery also => $

`$(function(){ });` 全写为 `$(document).ready(function(){ });` 意义为在DOM加载完毕后执行了ready()方法。


`(function(){})(jQuery);` 其实际上是执行`()(para)`匿名方法,只不过是传递了jQuery对象。

    jQuery(function(){　});
    用于存放操作DOM对象的代码，执行其中代码时DOM对象已存在。不可用于存放开发插件的代码，因为jQuery对象没有得到传递，外部通过jQuery.method也调用不了其中的方法（函数）。 
    (function(){　})(jQuery)；
    用于存放开发插件的代码，执行其中代码时DOM不一定存在，所以直接自动执行DOM操作的代码请小心使用。

在字符串中的每个新行 `\n` 之前插入 HTML 换行符 `<br />`
==
``` javascript
function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
```
json 转 array
==
``` javascript
function json2array(json){
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function(key){
        result.push(json[key]);
    });
    return result;
}
//example
json2array({a:1,b:2});
[1, 2]
//more complex example
json2array({20131013: 66, 20131014: 198, 20131015: 253, 20131016: 352, 20131017: 293, 20131018: 277, 20131019: 111, 20131020: 91, 20131021: 255, 20131022: 256, 20131023: 293, 20131024: 390, 20131025: 401, 20131026: 117, 20131027: 45, 20131028: 54, 20131029: 59, 20131030: 66, 20131031: 109, 20131101: 32});
[66, 198, 253, 352, 293, 277, 111, 91, 255, 256, 293, 390, 401, 117, 45, 54, 59, 66, 109, 32]
```

Max Array, Max Object
==
``` javascript
normal max usage:

Math.max(10, 20) returns 20.
Math.max(-10, -20) returns -10.
Math.max(-10, 20) returns 20.

Object {zh: 165790, foreign: 3706, other: 7207}

to get the max value of the Object:

Math.max.apply(null, json2array(o));


``` 

First Object
==
``` javascript
//to get the first ele of an Object
var maxPro = Object.keys(o)[0];

//or like this
for(ele in o){
    return o[ele];
}
``` 


string 转 array
==
``` javascript
"1,2".split(",").map(Number);
[1, 2]
"1,2".split(",");
["1", "2"]
JSON.parse("["+"1,2"+"]");
[1, 2]

```

规范化 JSON格式
==
``` javascript

//php print_r($response);

.post(url, params, function(data){
    console.log(data);//data并不是规范的JSON格式的
    data = $.parseJSON(data);
    console.log(data);
});


```

array 转 string
==
``` javascript
[1,2].join();
"1,2"
[1,2].join(",");
"1,2"
[1,2].join("|");
"1|2"
[1,2].toString();
"1,2"

[{a:"a"},{b:"b"}].toString();
"[object Object],[object Object]"
JSON.stringify([{a:"a"},{b:"b"}]);
"[{"a":"a"},{"b":"b"}]"
```

object/array translate
==
``` javascript
s = ["all", "zm", "fm", "zx"];
["all", "zm", "fm", "zx"]
dict = {"all":"总量", "zm":"正面", "fm":"负面", "zx":"中性"};
Object {all: "总量", zm: "正面", fm: "负面", zx: "中性"}
S = JSON.stringify(s);
"["all","zm","fm","zx"]"
for(var i in dict){ S = S.replace(i, dict[i]); };
"["总量","正面","负面","中性"]"
S = JSON.parse(S);
["总量", "正面", "负面", "中性"]

这里的 s 是简单数组，没必要这么复杂来解决翻译的问题。

但是如果 s 是一个对象数组，这个方法就比普通的遍历要简单吧。

var jsondict = {
    "all": "信息来源趋势",
    "zm": "正面",
    "fm": "负面",
    "zx": "中性",
};

function translateToZh(array_en){
    var str_zh = JSON.stringify(array_en);
    for(var i in jsondict){
        if(str_zh.indexOf(i) > 0){
            str_zh = str_zh.replace(eval("/"+i+"/g"), jsondict[i]);
        }
    };
    return JSON.parse(str_zh);
}

```

string.replace()
==
> stringObject.replace(regexp/substr,replacement)

`regexp/substr`: 规定子字符串或要替换的模式的 RegExp 对象。 请注意，如果该值是一个字符串，则将它作为要检索的直接量文本模式，而不是首先被转换为 RegExp 对象

`replacement`: 必需。一个字符串值。规定了替换文本或生成替换文本的函数。 

> 如果 regexp 具有全局标志 g，那么 replace() 方法将替换所有匹配的子串。否则，它只替换第一个匹配子串。
> replacement 可以是字符串，也可以是函数。如果它是字符串，那么每个匹配都将由字符串替换。但是 replacement 中的 $ 字符具有特定的含义。如下表所示，它说明从模式匹配得到的字符串将用于替换。

字符    替换文本

$1、$2、...、$99    与 regexp 中的第 1 到第 99 个子表达式相匹配的文本。

$&  与 regexp 相匹配的子串。

$`  位于匹配子串左侧的文本。

$'  位于匹配子串右侧的文本。

$$  直接量符号。

``` javascript
var str="Visit Microsoft!"
document.write(str.replace(/Microsoft/, "W3School"));

在本例中，我们将执行一次全局替换，每当 "Microsoft" 被找到，它就被替换为 "W3School"：
var str="Welcome to Microsoft! "
str=str + "We are proud to announce that Microsoft has "
str=str + "one of the largest Web Developers sites in the world."
document.write(str.replace(/Microsoft/g, "W3School"));

您可以使用本例提供的代码来确保匹配字符串大写字符的正确：
text = "javascript Tutorial";
text.replace(/javascript/i, "JavaScript");

在本例中，我们将把 "Doe, John" 转换为 "John Doe" 的形式：
name = "Doe, John";
name.replace(/(\w+)\s*, \s*(\w+)/, "$2 $1");

在本例中，我们将把字符串中所有单词的首字母都转换为大写：
name = 'aaa bbb ccc';
uw=name.replace(/\b\w+\b/g, function(word){
  return word.substring(0,1).toUpperCase()+word.substring(1);}
  );

``` 


正则中使用变量
==
法一、用 RegExp 对象。

``` javascript
var reStr = "all";
var re = new RegExp(reStr, "gi");
alert(str.replace(re, "全部"));
```

法二、用 eval。

``` javascript
var reStr = "all";
alert(str.replace(eval("/" + reStr + "/gi"), "全部"));
```

根据array中元素的value删除元素
==

``` javascript
var tag_story = [1,3,56,6,8,90],
    id_tag = 90,
    position = tag_story.indexOf(id_tag);

if ( ~position ) tag_story.splice(position, 1);
```


Note: IE < 9 does not support .indexOf() on arrays. If you want to make sure your code works in IE, you should use jQuery's $.inArray():

``` javascript
var tag_story = [1,3,56,6,8,90],
    id_tag = 90,
    position = $.inArray(id_tag, tag_story);

if ( ~position ) tag_story.splice(position, 1);
```

Object length
--

``` javascript
var a = {a:1,b:2,c:3,d:4};

//length(a);
function length(o) {
    var count = 0;
    for(var i in o){
        count ++;
    }
    return count;
};
alert(length(o));    

// a.length();
Object.prototype.length = function() {
    var count = 0;
    for(var i in this){
        count ++;
    }
    return count;
};
alert(a.length());    

//Object.size(a);
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
var size = Object.size(a);

```
