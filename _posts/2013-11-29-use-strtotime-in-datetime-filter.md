---
layout: post
title: "use strtotime in datetime filter"
description: "use strtotime in datetime filter"
keywords: "strtotime,datetime"
category: php
tags: [api]
---
{% include JB/setup %}

`$sdate`好处理，而`$edate`就比较麻烦，较安全的方法是:

<!-- more -->

`(strtotime("+1 day", strtotime($edate))-1)`

首先复习下`strtotime`函数

``` php
<?php 
php> echo strtotime("now")
1385706493
php> echo strtotime("3 Oct 2005")
1128268800
php> echo strtotime("3 October 2005")
1128268800
php> echo strtotime("+5hours")
1385724530
php> echo strtotime("+5 hours")
1385724534
php> echo strtotime("3October2005")
1128268800
php> echo strtotime("nextmonday")

php> echo strtotime("nextMonday")

php> echo strtotime("next Monday")
1385913600
php> 

```

下面来看个例子，这是一个时间起始选择控件

![datepicker]({{site.url}}/assets/img/2013/11/datepicker.png)

设定起始日期为`$sdate`, 终止日期为`$edate`

在写程序的时候

1，要考虑，用户只输入一个日期的情况
2，要考虑，`strtotime`的特性，它是秒级别的，不能简单的用`$sdate < time < $edate`

``` php
<?php 
if($sdate && $edate)
{
    $sphinxapi->SetFilterRange('public_time', strtotime($sdate), (strtotime("+1 day", strtotime($edate))-1), false);
}
elseif($sdate && !$edate)
{
    $sphinxapi->SetFilterRange('public_time', strtotime($sdate), time(), false);
}
elseif(!$sdate && $edate)
{
    $sphinxapi->SetFilterRange('public_time', strtotime('2013-01-01'), (strtotime("+1 day", strtotime($edate))-1), false);
}
else
{
    $sphinxapi->SetFilterRange('public_time', strtotime('2013-01-01'), time(), false);
}

```

`$sdate`好处理，而`$edate`就比较麻烦，较安全的方法是：

`(strtotime("+1 day", strtotime($edate))-1)`
