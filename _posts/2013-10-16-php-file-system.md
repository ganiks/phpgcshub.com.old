---
layout: post
tagline: "php file system"
title: "php文件处理"
description: ""
keywords: ""
category: code
tags: [php, file]
---
{% include JB/setup %}

+ 文件系统概述
+ 目录基本操作
+ 文件基本操作
+ 文件上传下载
+ 经典文件上传类
+ 小结

<!-- more -->
文件系统概述
==
1. PHP在Unix操作系统中可以获取7种文件类型
    + block     块设备（磁盘分区/CD-ROM）
    + char      字符设备（键盘/打印机/终端tty01）
    + **dir**       目录
    + fifo      命名管道（常用于将信息从一个进程传递给另一个进程）
    + **file**      普通文件
    + link      符号链接
    + unknown   

2. 跟文件类型有关的几个函数：
    + filetype()
    + is_file()
    + is_dir()
    + is_link()

3. 文件属性
    + file_exists()
    + filesize()
    + is_readable()
    + is_writable()
    + is_executable()
    + filectime()
    + filemtime()
    + fileatime()
    + stat()

> PHP会缓存文件属性信息，如果你要对同一个文件名进行多次操作并且需要该文件不被缓存，可以使用``clearstatcache()``函数

目录基本操作
==

1. 解析目录路径
    + ``DIRECTORY_SEPERATOR`` 在Unix中为``/``,在Windows中为``\``
    + ``$path = "/var/www/html/page.php"``
        + `basename($path)` =>page.php
        + `basename($path, ".php")` =>page
        + `dirname($path)` =>/var/www/html
        + `pathinfo($path)` => array('dirname'=>'/var/www/html', 'basename'=>'page.php', 'extension'=>'.php')
2. 遍历目录
    + $dir_handle = `opendir`('/var/www')
    + while($file = `readdir`($dir_handle))
    + `rewinddir`($dir_handle)
    + `closedir`($dir_handle)
    + foreach(`glob`("**.txt") as $file)
3. 统计目录大小
    + `filesize()`统计文件大小
    + `disk_free_space()`统计磁盘大小
    + `disk_total_space()`统计磁盘大小
    + 如何统计一个的目录
        + `exec()` or `system()` 调用操作系统命令`du`返回目录大小（但是出于安全考虑这些函数是禁用的，不利于跨平台）
        + 自定义一个递归函数来解决
4. 建立和删除目录
    + `mkdir()`建立一个新目录
    + `rmdir()`则只能删除一个存在的空目录
    + `unlink()`删除文件
    + 如何删除一个非空的目录
        + `exec()` or `system()` 调用操作系统命令`rm -rf`（但是出于安全考虑这些函数是禁用的，不利于跨平台）
        + 自定义一个递归函数来解决
5. 复制目录
    + `copy()`复制一个文件
    + 如何复制目录
        + `exec()` or `system()` 调用操作系统命令`cp -r`（但是出于安全考虑这些函数是禁用的，不利于跨平台）
        + 自定义一个递归函数来解决

文件基本操作
==

1. 文件的打开和关闭
    + `fopen`(**$filename**, **$mode**[, $use_include_path, $protos])
        + $handle = `fopen`("/home/liuyuan/123.txt", "r");
        + $handle = `fopen`("$_SERVER['DOCUMENT_ROOT']/data/123.txt", "r");
        + $handle = `fopen`("../../data/123.txt", "r");
        + $handle = `fopen`("http://www.phpgcs.com/123.txt", "r");
        + $handle = `fopen`("ftp://user:passwd@phpgcs.com/123.txt", "w");
        + $mode = r/w/x/a/b/t(只读/只写/创建并写头/创建并写尾/二进制模式b/文本模式t~~windows系统需要~~/)
        + $mode = r+/w+/x+/a+(读写/读写/创建并读写头/创建并读写尾)
        + $mode 如果文件已存在，w/w+ 将清空文件，而x/x+调用失败返回FALSE，报错E_WARNING
        + 函数返回的是一个资源句柄
    + `fclose($handle)`

2. 写入文件
    + int `fwrite`($handle, $string[, $length])
        + $string 注意换行`\n`
        + 返回的是写入的字符个数
    + `fputs()`是同名函数
    + `file_put_contents()`

3. 读取文件内容
    + `fread`($handle, $length)     读取打开的文件
    + `fgets`($handle, $length)     从打开的文件中返回一行
    + `fgetc`($handle, $length)     从打开的文件中返回字符
    + `file`($file)                 把文件读入一个数组
    + `file_get_contents`($file)    把文件读入一个字符串
    + `readfile`($file)             读取一个文件，并输出到输出缓冲`下载时很有用`

4. 访问远程文件
    + `allow_url_fopen`的配置选项必须打开
    + `http`协议只能用只读`r`模式打开
    + `ftp`协议可以用只读`r`和只写`w`模式打开

5. 移动文件指针
    + `ftell($handle)` 最开始是0
    + `fseek($handle, $offset, $whence)`
        + $whence = `SEEK_CUR`
        + $whence = `SEEK_END` $offset < 0
        + $whence = `SEEK_SET` 等同于未设置$whence

6. 文件锁定机制
    + `flock`

7. 文件的一些基本操作函数
    + `copy`($source, $destination)         复制文件
    + `unlink`($destination)                删除文件
    + `ftruncate`($destination, $length)    将文件截断到指定长度
    + `rename`($oldname, $newname)          重命名文件/目录

文件的上传下载
==

1. 文件上传
    + 客户端上传设置
    + `php.ini`配置
        + `file_uploads` = ON
        + `upload_max_filesize` = 2M (<post_max_size)
        + `post_max_size` = 8M
        + `upload_tmp_dir`
    + 上传信息`$_FILES['myfile']`
        + $_FILES['myfile']['`name`']
        + $_FILES['myfile']['`size`']
        + $_FILES['myfile']['`tmp_name`']
        + $_FILES['myfile']['`error`']
            + 0
            + 1 size> upload_max_filesize
            + 2 size> form's MAX_FILE_SIZE
            + 3 upload partly
            + 4 upload nothing
        + $_FILES['myfile']['`type`']   image/gif, text/html
    + 上传相关函数`is_uploaded_file()`和`move_uploaded_file()`
2. 批量上传
    + $FILE['myfile1']['name']
    + $FILE['myfile2']['name']
    + $FILE['myfile3']['name']
3. 文件下载

``` html

<form action="upload.php" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="100000">
    选择文件：<input type="file" name="myfile1">
    选择文件：<input type="file" name="myfile2">
    选择文件：<input type="file" name="myfile3">
    <input type="submit" value="上传">
</form>

$filename = "test.gif";

header('Content-Type: image/gif');
header('Content-Disposition: attachment; filename="'.$filename'"');
header('Content-Length: '.filesize($filename));

readfile($filename);

```

