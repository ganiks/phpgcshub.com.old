---
layout: post
title: "unserialize function unserialize error at offset"
description: "unserialize function unserialize error at offset"
keywords: "unserialize,php,offset,error"
category: php
tags: [error]
---
{% include JB/setup %}


Error at offset was dues to invalid serialization data due to invalid length

<!-- more -->

``` php
<?php 
public function actionExportexcel()
{
    $params['tab']  = Yii::app()->request->getParam('tab');
    $params['sel']  = Yii::app()->request->getParam('sel');
    $params['q']    = Yii::app()->request->getParam('q');
    $params['sort'] = Yii::app()->request->getParam('sort');
    $params         = serialize($params);

    print_r($params);
    //before

    $errorHandle    = 0;
    $filepath       = '';

    exec(Yii::app()->basePath."/yiic m_exportexcel patent --params=$params", $out, $status);
    print_r($out[0]);exit;
    ... ...

<?php
class M_exportexcelCommand extends CConsoleCommand
{
    public $debug=FALSE;

    public function actionPatent($params='')
    {
        $errorHandle= FALSE;
        print_r($params);exit;
        //after
        $params     = unserialize($params);
```

error like this:

PHP Error[8]: unserialize(): Error at offset 5 of 12 bytes

I print `$params` before and after exec function

``` json
//before
a:4:{s:3:"tab";s:6:"patent";s:3:"sel";s:2:"zh";s:1:"q";s:17:"sub:(A1_3D打印)";s:4:"sort";s:9:"atime.asc";}

//after
a:4:{s:3:tab
```

ok, the problem was caused by the `"`

to solve ,I should wrap `$param` with `'` like this:

``` php
<?php 

exec(Yii::app()->basePath."/yiic m_exportexcel patent --params='$params'", $out, $status);

```

refer: <a target="_blank" href="http://stackoverflow.com/questions/10152904/unserialize-function-unserialize-error-at-offset">http://stackoverflow.com/questions/10152904/unserialize-function-unserialize-error-at-offset</a>



