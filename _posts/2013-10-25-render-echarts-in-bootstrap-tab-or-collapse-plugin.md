---
layout: post
title: "render echarts in bootstrap tab or collapse plugin"
description: ""
keywords: "echarts, bootstrap, collapse, tab page"
category: code 
tags: [echarts]
---
{% include JB/setup %}

When I use echarts together with bootstrap-collapse(折叠/手风琴) or bootstrap-tab(标签页) plugins (which have hidden divs at first), I found that I can't normally render echarts on those hiddened divs .

<!-- more -->
Through re-initing echarts again when the hiddened divs shown , the problem get solved.

首先是Tab控件

``` html
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab_2_1" data-toggle="tab" id="sentimentTab_all" name="all">总体</a></li>
    <li><a href="#tab_2_2" data-toggle="tab" id="sentimentTab_news" name="news">新闻</a></li>
    <li><a href="#tab_2_3" data-toggle="tab" id="sentimentTab_weibo" name="weibo">微博</a></li>
</ul>
... ...
... ...
... ...

<div id="sentimentPie_all" class="chart" style="padding: 0px; position: relative; height: 320px;">111</div>
<div id="sentimentPie_news" class="chart" style="padding: 0px; position: relative; height: 320px;">111</div>
<div id="sentimentPie_weibo" class="chart" style="padding: 0px; position: relative; height: 320px;">111</div>
```

下面是常规的使用echarts实现pie，会遇到本文最开始提的问题：

pie(all)是正常显示的，但是tab切换后是没有图形的(news/weibo)，也就是说页面加载完毕时未激活（隐藏）的tab(news/weibo)是没有render图形的

``` javascript
var sentimentPie, sentimentPieOption={}, sentimentPieChart = {
    all: document.getElementById('sentimentPie_all'),
    news: document.getElementById('sentimentPie_news'),
    weibo: document.getElementById('sentimentPie_weibo'),
};

require.config({
    packages: [
        {
            name: 'echarts',
            location: '../../../../media/echarts/src',
            main: 'echarts'
        },
        {
            name: 'zrender',
            //location: 'http://ecomfe.github.io/zrender/src',
            location: '../../../../media/zrender/src',
            main: 'zrender'
        }
    ]
});

 function requireConfig() {
     // 按需加载
     require(
         [
             'echarts',
             'echarts/chart/pie',
         ],
         requireCallback
     );
 }
 function requireCallback (echarts) {
     sentimentPie = {
         all: echarts.init(sentimentPieChart.all),
         news: echarts.init(sentimentPieChart.news),
         weibo: echarts.init(sentimentPieChart.weibo),
     };
 }
 requireConfig();

sentimentPieOption[type] ={
    ....
}

sentimentPie[type].setOption(sentimentPieOption[type], true);

```

下面的代码解决了这个问题：

``` javascript
//specially re-init and render echarts for bootstrap-tab plugin
$('a[id^="sentimentTab_"]').on('shown', function(e){
    require('echarts').init(sentimentPieChart[$(this).attr('name')]).setOption(sentimentPieOption[$(this).attr('name')], true);
});

```

最后再次强调echarts的引入/初始化/实例化的流程[(官方API)](http://ecomfe.github.io/echarts/doc/doc.html#引入ECharts)

不管如何引入ECharts，都需要如下4步：

1. 引入一个模块加载器，如esl.js或者require.js
2. 为ECharts准备一个具备大小（宽高）的Dom（当然可以是动态生成的）
3. 为模块加载器配置echarts的路径，从当前页面链接到echarts.js，见上述说明
4. 动态加载echarts然后在回调函数中开始使用
 

> 当你确保同一页面已经加载过echarts，再使用时直接require('echarts').init(dom)就行,正如本文一样。
